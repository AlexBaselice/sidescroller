######################################################
#
# Top Level Code Overview
# Lines before if __name__ are run once at the start
# Lines after if are run once per each frame update
#
######################################################

# Importing standard modules
import soy
import time
import sys

from time import sleep

# Importing custom modules
from inital import menu
from inital import level1
from inital import models
from inital import texture
from inital import enemy1
from inital import enemy2
from inital import enemy3
from inital import level1Models
from inital import level1ModelLoader
from inital import level1MovementEnforcer
from inital import level2Models
from inital import level2ModelLoader
from inital import level2MovementEnforcer


from runtime import camera
from runtime import movement
from runtime import selection
from runtime import menuRuntime
from runtime import endgame
from runtime import player
from runtime import score


# Intitalize Settings to control game debugging behavior
# Debug Var for optional text information to console
debug = 0
# Add debug var for debug camera
# FastLoadLevel1 -Loads level 1 with minimal loading times for testing
fastLoad = 0

# Load all textures first for loading speed. Ensure that textures are not loaded again in functions
textures = dict()
# Load Materials first
materials = dict()

# Level Loading Settings
roomWidth = 400

# Loading Room, Client, and Camera
room = menu.createRoom(roomWidth)
client = menu.createClient(room)

# Loading Loading screen before loading textures to avoid long unresponsive wait
texture.loadLoadingTexture(textures, fastLoad)
menu.addLoadingScreen(room, textures)
menu.mainMenuInit(room, textures)

# Loading level
texture.loadTextures(textures, fastLoad)
level1.levelOneInit(room, textures, roomWidth)
enemy1.createEnemy(room)

# Loading level 1 Models from Blender
level1ModelLoader.level1LoadModels(room, textures)

# Finished loading, Remove loading screen and bring select block forward
menu.changeLoadingScreenToMainMenuBackground(room, textures)
room['selectionBlock'].position = soy.atoms.Position((0, -3, 22))

playerHasSlectedMainMenu = 0

# Setting the menu as open
menuOpen = 1
menuRuntime.selectedLevel(room, menuOpen)
gameState = dict()
gameState['currentLevel'] = 1

# Setting game specific vars
cameraScrollSpeedX = 4.1
cameraScrollSpeedY = 8

collisionRangeX = 4
collisionRangeY = 3
collisionRangeZ = 2

previousFrameYforWorS = -3.0
previousFrameYforAorD = -3.0

#Loading level2 Enemy
enemy2.createEnemy(room)

#Loading Final Boss
enemy3.createEnemy(room)

#Make sure to prevent environment from moving before done loading
movement.preventEnvironmentMoving(room, gameState)

if __name__ == '__main__':
    while client.window:
        # Menu function exits early if not open
        menuOpen = menuRuntime.selectedLevel(room, menuOpen)
        menuOpen = endgame.checkIfplayerOutOfBounds(room, textures, menuOpen, client)
        camera.cameraAlignWithPlayer(room, cameraScrollSpeedX, cameraScrollSpeedY, debug, menuOpen)
        # Preventing objects from moving - Would like to see toggle_immovable() working for bodies
        movement.preventEnvironmentMoving(room, gameState)
        player.gravitySwitch(room, debug)
        score.updateScore(client, room, menuOpen)
        # Prevent meshes from blender from moving
        if gameState['currentLevel'] == 1:
            level1MovementEnforcer.freezeSceneMeshes(room)
        # Only do these actions if the player is added to the game, the menu is not open
        if menuOpen == 0:
            previousFrameYforWorS = player.pressedWorS(room, previousFrameYforWorS, debug)
            previousFrameYforAorD = player.pressedAorD(room, previousFrameYforAorD, debug)
            # Prevent the player form moving out of bounds
            movement.preventFromRotating(room['player'])
            player.returnPlayerUpright(room)
            player.slowDownPlayer(room)
            enemy3.changeRoomGravityRandomly(room)
            # Check if player collides with any enemies, probably a better way to do this, bad code smell
            if menuOpen == 0:
                menuOpen = player.checkIfPlayerCollidesWithEnemy(room, collisionRangeX, collisionRangeY, collisionRangeZ, textures, gameState, client)
            if menuOpen == 0:
                menuOpen = player.checkIfPlayerCollidesWithEnemy2(room, collisionRangeX, collisionRangeY, collisionRangeZ, textures, gameState, client)
            if menuOpen == 0:
                menuOpen = player.checkIfPlayerCollidesWithEnemy3(room, collisionRangeX, collisionRangeY, collisionRangeZ, textures, gameState, client)
            if menuOpen == 0:
                menuOpen = movement.preventPlayerFromLeavingPlayVolume(room['player'], 14, 0, -65, 118, room, textures, gameState, client)

        sleep(.1)
