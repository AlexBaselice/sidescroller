# Contains functions for Menu initialization

import soy


# Move functions not relateing to Menu out of Menu model
def createRoom(roomWidth):
    return soy.scenes.Room(soy.atoms.Size((roomWidth, 280.0, 400.0)), 0.5, 0.0, 0.0, 7.0)

def createClient(room):
    client = soy.Client()
    client.window.background = soy.atoms.Color('blue')
    client.window.title = "SideScroller"

    # Camera and Environment Settings
    room['cam'] = soy.bodies.Camera(soy.atoms.Position((0, -3, 32)))
    client.window.append(soy.widgets.Projector(room['cam']))
    return client

def mainMenuInit(room, textures):
    # Create Menu Overlay
    # Create Create Menu Block Moves to select Play
    gold = soy.atoms.Color('#ffffff')
    firebrick = soy.atoms.Color('firebrick')

    cubemap = soy.textures.Cubemap("checkered", [gold, firebrick], 1, 1, 1)
    # Return a starting position, use this to keep in place each frame
    room['playBlock'] = soy.bodies.Box()
    room['playBlock'].position = soy.atoms.Position((0, -1, -22))
    room['playBlock'].material = soy.materials.Textured()
    room['playBlock'].material.colormap = textures['play']
    room['playBlock'].size = soy.atoms.Size((3, 2, 1))
    room['playBlock'].radius = 0.1
    room['playBlock'].toggleField()

    # Create Quit Block
    room['quitBlock'] = soy.bodies.Box()
    room['quitBlock'].position = soy.atoms.Position((0, -5, -22))
    room['quitBlock'].material = soy.materials.Textured()
    room['quitBlock'].material.colormap = textures['quit']
    room['quitBlock'].size = soy.atoms.Size((3, 2, 1))
    room['quitBlock'].radius = 0.1
    room['quitBlock'].toggleField()

    # Create Selection Spehere
    room['selectionBlock'] = soy.bodies.Sphere()
    room['selectionBlock'].position = soy.atoms.Position((0, -3, -22))
    room['selectionBlock'].material = soy.materials.Textured()
    room['selectionBlock'].material.colormap = cubemap
    room['selectionBlock'].radius = 0.5
    room['selectionBlock'].toggleField()

	# soy.events.KeyPress.addAction("Down", BThrust)
	# soy.events.KeyRelease.addAction("Down", FThrust)

    # Set Up Movement With Selection Block
    return


def addLoadingScreen(room, textures):
    loadingMat = soy.materials.Textured()
    loadingMat.glowmap = textures['loading']
    loadingMat.shininess = 100
    room['loadingBillboard'] = soy.bodies.Billboard(soy.atoms.Position((0, -3, 12)),
                                                    size=soy.atoms.Size((30, 20)), material=loadingMat)
    room['loadingBillboard'].density = 1000
    return


def changeLoadingScreenBackgroundTexture(room, textures):
    loadingMat = soy.materials.Textured()
    loadingMat.shininess = 100
    loadingMat.glowmap = textures['sky']
    room['loadingBillboard'].material = loadingMat
    return


def changeLoadingScreenToMainMenuBackground(room, textures):
    mainMenuMat = soy.materials.Textured()
    mainMenuMat.glowmap = textures['mainMenu']
    mainMenuMat.shininess = 100
    room['loadingBillboard'] = soy.bodies.Billboard(soy.atoms.Position((0, -3, 12)),
                                                    size=soy.atoms.Size((30, 20)), material=mainMenuMat)
    return


def removeLoadingScreen(room):
    room['loadingBillboard'] = soy.bodies.Billboard(soy.atoms.Position((0, -3, 12)),
                                                    size=soy.atoms.Size((0, 0)), material=soy.materials.Textured())
    return
