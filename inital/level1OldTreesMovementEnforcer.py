import soy
def freezeMesh(item, position, rotation):
    item.velocity = soy.atoms.Vector((0,0,0))
    item.angular_velocity = soy.atoms.Vector((0,0,0))
    item.position = position
    item.rotation = rotation
    return
def freezeSceneMeshes(room):
    freezeMesh(room["NoMove_Tree4"], soy.atoms.Position((-67.12344360351562,-4.2578349113464355,-6.705403804779053)),soy.atoms.Rotation((0.5046827793121338,-0.0,1.5708)))
    freezeMesh(room["NoMove_GroundPlane"], soy.atoms.Position((-37.246456146240234,-17.572589874267578,-51.379737854003906)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["RP_Cube"], soy.atoms.Position((-60.63926696777344,-8.715718269348145,1.7081859111785889)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["NoMove_Mountain3"], soy.atoms.Position((-62.044456481933594,-18.535690307617188,-63.39704513549805)),soy.atoms.Rotation((0.032474443316459656,0.0,1.5708)))
    freezeMesh(room["RP_Cube2"], soy.atoms.Position((-38.167579650878906,-8.715718269348145,1.7081859111785889)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["RP_Cube3"], soy.atoms.Position((-7.210450172424316,-8.715718269348145,1.7081859111785889)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["RP_Cube4"], soy.atoms.Position((-25.798484802246094,-14.254152297973633,1.7081859111785889)),soy.atoms.Rotation((0.0,-4.723508358001709,1.5708)))
    freezeMesh(room["RP_Cube5"], soy.atoms.Position((-7.9078240394592285,-0.6940286755561829,1.1830615997314453)),soy.atoms.Rotation((0.0,-4.723508358001709,1.5708)))
    freezeMesh(room["RP_Cube6"], soy.atoms.Position((-6.649463653564453,11.53433609008789,1.7081859111785889)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["NoMove_Mountain5"], soy.atoms.Position((-49.77031707763672,-22.739730834960938,-33.95873260498047)),soy.atoms.Rotation((-0.3485445976257324,0.0,1.5708)))
    return
