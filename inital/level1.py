# Contains functions for level1 initialization
import soy
import sys

from math import sqrt

def levelOneInit(room, textures, roomWidth) :
    # Events init
    soy.events.KeyPress.init()
    soy.events.KeyRelease.init()

    ## Lighting ##
    # Sun
    room['sun'] = soy.bodies.Sun(8)

    # Old Yellow Sun Color
    room['sun'].diffuse = soy.atoms.Color('#FDB813')
    room['sun'].specular = soy.atoms.Color('#FDB813')

    # Point Light - Still needed for menu?
    #room['light'] = soy.bodies.Light(soy.atoms.Position((-2, 3, 20)))
    #room['light'].diffuse = soy.atoms.Color('#FDB813')

    # Physics
    gravity = soy.fields.Accelerate(soy.atoms.Vector((0, -9.8, 0)))
    room.addField('gravity', gravity)

    # Environment Setings
    groundLevel = -7

    gold = soy.atoms.Color('#ccaaff')
    firebrick = soy.atoms.Color('firebrick')

    cubemap = soy.textures.Cubemap("checkered", [gold, firebrick], 1, 1, 1)

    # Gravity Selection Block

    room['gravitySelectionBlock'] = soy.bodies.Box()
    room['gravitySelectionBlock'].position = soy.atoms.Position((-4, -3, 35))
    room['gravitySelectionBlock'].material = soy.materials.Textured()
    room['gravitySelectionBlock'].material.colormap = cubemap
    room['gravitySelectionBlock'].size = soy.atoms.Size((1, 1, 1))
    room['gravitySelectionBlock'].toggleField()

    #W or S selection block - where is this in view port?
    room['selectionBlockWorS'] = soy.bodies.Box()
    room['selectionBlockWorS'].position = soy.atoms.Position((-5.5, -3, -35))
    room['selectionBlockWorS'].material = soy.materials.Textured()
    room['selectionBlockWorS'].material.colormap = cubemap
    room['selectionBlockWorS'].size = soy.atoms.Size((1, 1, 1))
    room['selectionBlockWorS'].toggleField()

    # A or S selection block - move this section to when player is initiated
    room['selectionBlockAorD'] = soy.bodies.Box()
    room['selectionBlockAorD'].position = soy.atoms.Position((2, -3, -35))
    room['selectionBlockAorD'].material = soy.materials.Textured()
    room['selectionBlockAorD'].material.colormap = cubemap
    room['selectionBlockAorD'].size = soy.atoms.Size((1, 1, 1))

    room['selectionBlockAorD'].toggleField()

    # Sky
    skymat = soy.materials.Textured()
    skymat.glowmap = textures['sky']
    skymat.shininess = 100
    # room['billboard'] = soy.bodies.Billboard(soy.atoms.Position((0, 3, -20)),
    #										 size=soy.atoms.Size((40, 15)), material=skymat)
    # room['billboard'].density = 1000
    room.material = skymat

    # Ground
    grassmat = soy.materials.Textured()
    grassmat.colormap = textures['grass']


    # Camera controls For Debugging. Will Not Be In Final Release
    RCamforce = soy.atoms.Vector((0, -40, 0))  # Right force
    LCamforce = soy.atoms.Vector((0, 40, 0))  # Left force
    UCamforce = soy.atoms.Vector((40, 0, 0))  # Up force
    DCamforce = soy.atoms.Vector((-40, 0, 0))  # Down force
    FCamforce = soy.atoms.Vector((0, 0, -200))  # Forwards force
    BCamforce = soy.atoms.Vector((0, 0, 200))  # Backwards force
    LeftCamMovForce = soy.atoms.Vector((-200, 0, 0))  # Left Move
    RightCamMovForce = soy.atoms.Vector((200, 0, 0))  # Right Move

    # Actions
    RCamThrust = soy.actions.Rotate(room['cam'], RCamforce)
    LCamThrust = soy.actions.Rotate(room['cam'], LCamforce)
    UCamThrust = soy.actions.Rotate(room['cam'], UCamforce)
    DCamThrust = soy.actions.Rotate(room['cam'], DCamforce)
    FCamThrust = soy.actions.Thrust(room['cam'], FCamforce)
    BCamThrust = soy.actions.Thrust(room['cam'], BCamforce)
    LeftCamMov = soy.actions.Thrust(room['cam'], LeftCamMovForce)
    RightCamMov = soy.actions.Thrust(room['cam'], RightCamMovForce)

    # Camera KeyPress Events
    soy.events.KeyPress.addAction("Right", RCamThrust)
    soy.events.KeyRelease.addAction("Right", LCamThrust)
    soy.events.KeyPress.addAction("Left", LCamThrust)
    soy.events.KeyRelease.addAction("Left", RCamThrust)
    soy.events.KeyPress.addAction("I", FCamThrust)
    soy.events.KeyRelease.addAction("I", BCamThrust)
    soy.events.KeyPress.addAction("K", BCamThrust)
    soy.events.KeyRelease.addAction("K", FCamThrust)
    soy.events.KeyPress.addAction("L", LeftCamMov)
    soy.events.KeyRelease.addAction("L", RightCamMov)
    soy.events.KeyPress.addAction("J", RightCamMov)
    soy.events.KeyRelease.addAction("J", LeftCamMov)

    # Selection Box Key Press - Should move these to a seperate file?
    # Movement Speed value or selection bock speed

    selectionSpeed = 100
    USelectionForce = soy.atoms.Vector((0, selectionSpeed, 0))  # Up force
    DSelectionForce = soy.atoms.Vector((0, -selectionSpeed, 0))  # Down force

    USelectionThrust = soy.actions.Thrust(room['selectionBlock'], USelectionForce )
    DSelectionThrust = soy.actions.Thrust(room['selectionBlock'], DSelectionForce )

    # Selection KeyPress Events
    soy.events.KeyPress.addAction("Up", USelectionThrust)
    soy.events.KeyRelease.addAction("Up", DSelectionThrust)
    soy.events.KeyPress.addAction("Down", DSelectionThrust)
    soy.events.KeyRelease.addAction("Down", USelectionThrust)

    # Gravity selection Key Press
    gravitySelectionSpeed = 500
    UGravitySelectionForce = soy.atoms.Vector((0, gravitySelectionSpeed, 0))  # Up force
    DGravitySelectionForce = soy.atoms.Vector((0, -gravitySelectionSpeed, 0))  # Down force

    UGravitySelectionThrust = soy.actions.Thrust(room['gravitySelectionBlock'], UGravitySelectionForce )
    DGravitySelectionThrust = soy.actions.Thrust(room['gravitySelectionBlock'], DGravitySelectionForce )

    # Selection KeyPress Events
    soy.events.KeyPress.addAction("V", UGravitySelectionThrust)
    soy.events.KeyRelease.addAction("V", DGravitySelectionThrust)
    soy.events.KeyPress.addAction("C", DGravitySelectionThrust)
    soy.events.KeyRelease.addAction("C", UGravitySelectionThrust)

    # W or S keyPress
    selectionSpeedWorS = 100
    USelectionForceWorS = soy.atoms.Vector((0, selectionSpeedWorS, 0))  # Up force
    DSelectionForceWorS = soy.atoms.Vector((0, -selectionSpeedWorS, 0))  # Down force

    UGravitySelectionThrustWorS = soy.actions.Thrust(room['selectionBlockWorS'], USelectionForceWorS)
    DGravitySelectionThrustWorS = soy.actions.Thrust(room['selectionBlockWorS'], DSelectionForceWorS)

    # Selection KeyPress Events
    soy.events.KeyPress.addAction("W", UGravitySelectionThrustWorS)
    soy.events.KeyRelease.addAction("W", DGravitySelectionThrustWorS)
    soy.events.KeyPress.addAction("S", DGravitySelectionThrustWorS)
    soy.events.KeyRelease.addAction("S", UGravitySelectionThrustWorS)

    # A or D key Press Events
    selectionSpeedAorD = 100
    USelectionForceAorD = soy.atoms.Vector((0, selectionSpeedAorD, 0))  # Up force
    DSelectionForceAorD = soy.atoms.Vector((0, -selectionSpeedAorD, 0))  # Down force

    UGravitySelectionThrust = soy.actions.Thrust(room['selectionBlockAorD'], USelectionForceAorD)
    DGravitySelectionThrust = soy.actions.Thrust(room['selectionBlockAorD'], DSelectionForceAorD)

    # Selection KeyPress Events
    soy.events.KeyPress.addAction("A", UGravitySelectionThrust)
    soy.events.KeyRelease.addAction("A", DGravitySelectionThrust)
    soy.events.KeyPress.addAction("D", DGravitySelectionThrust)
    soy.events.KeyRelease.addAction("D", UGravitySelectionThrust)


    return
