import soy

from math import sqrt

def addTree(room, key, position, textures) :
    room[key] = soy.bodies.Mesh()

    matIndex1 = soy.materials.Colored("green")
    matIndex0 = soy.materials.Colored("brown")
    room[key].rotation = soy.atoms.Rotation((0, 0, 1.5))

    room[key].position = position
    room[key].toggleField()

    v0 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, -1.0)), soy.atoms.Vector((0.0, 0.0, -1.0)),
                          soy.atoms.Position((0.3432457447052002, 0.06220778077840805)), soy.atoms.Vector((0, 0, 0)))
    v1 = soy.atoms.Vertex(soy.atoms.Position((0.0, 1.0, -1.0)),
                          soy.atoms.Vector((0.0, 0.825678288936615, -0.5641041398048401)),
                          soy.atoms.Position((0.3339645564556122, 0.04476860910654068)), soy.atoms.Vector((0, 0, 0)))
    v2 = soy.atoms.Vertex(soy.atoms.Position((0.7071067690849304, 0.7071067690849304, -1.0)),
                          soy.atoms.Vector((0.5623035430908203, 0.5871761441230774, -0.5822321176528931)),
                          soy.atoms.Position((0.34557223320007324, 4.8266916564898565e-05)),
                          soy.atoms.Vector((0, 0, 0)))
    v3 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, 3.9740030765533447)),
                          soy.atoms.Vector((-0.029663991183042526, 0.012268440797924995, 0.999481201171875)),
                          soy.atoms.Position((0.34243789315223694, 0.9999517202377319)), soy.atoms.Vector((0, 0, 0)))
    v4 = soy.atoms.Vertex(soy.atoms.Position((0.5658128261566162, 0.5658128261566162, 1.0836149454116821)),
                          soy.atoms.Vector((0.5980101823806763, 0.7817010879516602, 0.17694631218910217)),
                          soy.atoms.Position((0.3565353453159332, 0.20357738435268402)), soy.atoms.Vector((0, 0, 0)))
    v5 = soy.atoms.Vertex(soy.atoms.Position((-1.190366027969958e-09, 0.640231728553772, 1.052250623703003)),
                          soy.atoms.Vector((-0.11682485044002533, 0.9783623814582825, 0.17069002985954285)),
                          soy.atoms.Position((0.35524094104766846, 0.20218642055988312)), soy.atoms.Vector((0, 0, 0)))
    v6 = soy.atoms.Vertex(soy.atoms.Position((-1.190366027969958e-09, 0.640231728553772, 1.052250623703003)),
                          soy.atoms.Vector((-0.11682485044002533, 0.9783623814582825, 0.17069002985954285)),
                          soy.atoms.Position((0.35524094104766846, 0.20218642055988312)), soy.atoms.Vector((0, 0, 0)))
    v7 = soy.atoms.Vertex(soy.atoms.Position((0.7071067690849304, 0.7071067690849304, -1.0)),
                          soy.atoms.Vector((0.5623035430908203, 0.5871761441230774, -0.5822321176528931)),
                          soy.atoms.Position((0.34557223320007324, 4.8266916564898565e-05)),
                          soy.atoms.Vector((0, 0, 0)))
    v8 = soy.atoms.Vertex(soy.atoms.Position((0.0, 1.0, -1.0)),
                          soy.atoms.Vector((0.0, 0.825678288936615, -0.5641041398048401)),
                          soy.atoms.Position((0.3339645564556122, 0.04476860910654068)), soy.atoms.Vector((0, 0, 0)))
    v9 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, -1.0)), soy.atoms.Vector((0.0, 0.0, -1.0)),
                          soy.atoms.Position((0.3432457447052002, 0.06220778077840805)), soy.atoms.Vector((0, 0, 0)))
    v10 = soy.atoms.Vertex(soy.atoms.Position((0.7071067690849304, 0.7071067690849304, -1.0)),
                           soy.atoms.Vector((0.5623035430908203, 0.5871761441230774, -0.5822321176528931)),
                           soy.atoms.Position((0.34557223320007324, 4.8266916564898565e-05)),
                           soy.atoms.Vector((0, 0, 0)))
    v11 = soy.atoms.Vertex(soy.atoms.Position((1.0, -4.371138828673793e-08, -1.0)),
                           soy.atoms.Vector((0.8044068813323975, 0.0, -0.5940427780151367)),
                           soy.atoms.Position((0.3559127449989319, 0.049157705157995224)), soy.atoms.Vector((0, 0, 0)))
    v12 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, 3.9740030765533447)),
                           soy.atoms.Vector((-0.029663991183042526, 0.012268440797924995, 0.999481201171875)),
                           soy.atoms.Position((0.34243789315223694, 0.9999517202377319)), soy.atoms.Vector((0, 0, 0)))
    v13 = soy.atoms.Vertex(soy.atoms.Position((0.8001823425292969, -2.7985416650722073e-08, 1.0836149454116821)),
                           soy.atoms.Vector((0.9839777946472168, 0.0, 0.17813654243946075)),
                           soy.atoms.Position((0.3503111004829407, 0.20928388833999634)), soy.atoms.Vector((0, 0, 0)))
    v14 = soy.atoms.Vertex(soy.atoms.Position((0.5658128261566162, 0.5658128261566162, 1.0836149454116821)),
                           soy.atoms.Vector((0.5980101823806763, 0.7817010879516602, 0.17694631218910217)),
                           soy.atoms.Position((0.3565353453159332, 0.20357738435268402)), soy.atoms.Vector((0, 0, 0)))
    v15 = soy.atoms.Vertex(soy.atoms.Position((0.5658128261566162, 0.5658128261566162, 1.0836149454116821)),
                           soy.atoms.Vector((0.5980101823806763, 0.7817010879516602, 0.17694631218910217)),
                           soy.atoms.Position((0.3565353453159332, 0.20357738435268402)), soy.atoms.Vector((0, 0, 0)))
    v16 = soy.atoms.Vertex(soy.atoms.Position((1.0, -4.371138828673793e-08, -1.0)),
                           soy.atoms.Vector((0.8044068813323975, 0.0, -0.5940427780151367)),
                           soy.atoms.Position((0.3559127449989319, 0.049157705157995224)), soy.atoms.Vector((0, 0, 0)))
    v17 = soy.atoms.Vertex(soy.atoms.Position((0.7071067690849304, 0.7071067690849304, -1.0)),
                           soy.atoms.Vector((0.5623035430908203, 0.5871761441230774, -0.5822321176528931)),
                           soy.atoms.Position((0.34557223320007324, 4.8266916564898565e-05)),
                           soy.atoms.Vector((0, 0, 0)))
    v18 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, -1.0)), soy.atoms.Vector((0.0, 0.0, -1.0)),
                           soy.atoms.Position((0.3432457447052002, 0.06220778077840805)), soy.atoms.Vector((0, 0, 0)))
    v19 = soy.atoms.Vertex(soy.atoms.Position((1.0, -4.371138828673793e-08, -1.0)),
                           soy.atoms.Vector((0.8044068813323975, 0.0, -0.5940427780151367)),
                           soy.atoms.Position((0.3559127449989319, 0.049157705157995224)), soy.atoms.Vector((0, 0, 0)))
    v20 = soy.atoms.Vertex(soy.atoms.Position((0.7071067690849304, -0.7071067690849304, -1.0)),
                           soy.atoms.Vector((0.5688039660453796, -0.5688039660453796, -0.5940427780151367)),
                           soy.atoms.Position((0.3535991907119751, 0.07528285682201385)), soy.atoms.Vector((0, 0, 0)))
    v21 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, 3.9740030765533447)),
                           soy.atoms.Vector((-0.029663991183042526, 0.012268440797924995, 0.999481201171875)),
                           soy.atoms.Position((0.34243789315223694, 0.9999517202377319)), soy.atoms.Vector((0, 0, 0)))
    v22 = soy.atoms.Vertex(soy.atoms.Position((0.5658128261566162, -0.5658128261566162, 1.0836149454116821)),
                           soy.atoms.Vector((0.695791482925415, -0.695791482925415, 0.17813654243946075)),
                           soy.atoms.Position((0.34243783354759216, 0.21376270055770874)), soy.atoms.Vector((0, 0, 0)))
    v23 = soy.atoms.Vertex(soy.atoms.Position((0.8001823425292969, -2.7985416650722073e-08, 1.0836149454116821)),
                           soy.atoms.Vector((0.9839777946472168, 0.0, 0.17813654243946075)),
                           soy.atoms.Position((0.3503111004829407, 0.20928388833999634)), soy.atoms.Vector((0, 0, 0)))
    v24 = soy.atoms.Vertex(soy.atoms.Position((0.8001823425292969, -2.7985416650722073e-08, 1.0836149454116821)),
                           soy.atoms.Vector((0.9839777946472168, 0.0, 0.17813654243946075)),
                           soy.atoms.Position((0.3503111004829407, 0.20928388833999634)), soy.atoms.Vector((0, 0, 0)))
    v25 = soy.atoms.Vertex(soy.atoms.Position((0.7071067690849304, -0.7071067690849304, -1.0)),
                           soy.atoms.Vector((0.5688039660453796, -0.5688039660453796, -0.5940427780151367)),
                           soy.atoms.Position((0.3535991907119751, 0.07528285682201385)), soy.atoms.Vector((0, 0, 0)))
    v26 = soy.atoms.Vertex(soy.atoms.Position((1.0, -4.371138828673793e-08, -1.0)),
                           soy.atoms.Vector((0.8044068813323975, 0.0, -0.5940427780151367)),
                           soy.atoms.Position((0.3559127449989319, 0.049157705157995224)), soy.atoms.Vector((0, 0, 0)))
    v27 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, -1.0)), soy.atoms.Vector((0.0, 0.0, -1.0)),
                           soy.atoms.Position((0.3432457447052002, 0.06220778077840805)), soy.atoms.Vector((0, 0, 0)))
    v28 = soy.atoms.Vertex(soy.atoms.Position((0.7071067690849304, -0.7071067690849304, -1.0)),
                           soy.atoms.Vector((0.5688039660453796, -0.5688039660453796, -0.5940427780151367)),
                           soy.atoms.Position((0.3535991907119751, 0.07528285682201385)), soy.atoms.Vector((0, 0, 0)))
    v29 = soy.atoms.Vertex(soy.atoms.Position((-8.742277657347586e-08, -1.0, -1.0)),
                           soy.atoms.Vector((-0.01757866144180298, -0.8127994537353516, -0.5822321176528931)),
                           soy.atoms.Position((0.3474087119102478, 0.08671217411756516)), soy.atoms.Vector((0, 0, 0)))
    v30 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, 3.9740030765533447)),
                           soy.atoms.Vector((-0.029663991183042526, 0.012268440797924995, 0.999481201171875)),
                           soy.atoms.Position((0.34243789315223694, 0.9999517202377319)), soy.atoms.Vector((0, 0, 0)))
    v31 = soy.atoms.Vertex(soy.atoms.Position((-5.716119844123568e-08, -0.8001823425292969, 1.0836149454116821)),
                           soy.atoms.Vector((-0.12988677620887756, -0.9755851626396179, 0.17694631218910217)),
                           soy.atoms.Position((0.3328333795070648, 0.2146204710006714)), soy.atoms.Vector((0, 0, 0)))
    v32 = soy.atoms.Vertex(soy.atoms.Position((0.5658128261566162, -0.5658128261566162, 1.0836149454116821)),
                           soy.atoms.Vector((0.695791482925415, -0.695791482925415, 0.17813654243946075)),
                           soy.atoms.Position((0.34243783354759216, 0.21376270055770874)), soy.atoms.Vector((0, 0, 0)))
    v33 = soy.atoms.Vertex(soy.atoms.Position((0.5658128261566162, -0.5658128261566162, 1.0836149454116821)),
                           soy.atoms.Vector((0.695791482925415, -0.695791482925415, 0.17813654243946075)),
                           soy.atoms.Position((0.34243783354759216, 0.21376270055770874)), soy.atoms.Vector((0, 0, 0)))
    v34 = soy.atoms.Vertex(soy.atoms.Position((-8.742277657347586e-08, -1.0, -1.0)),
                           soy.atoms.Vector((-0.01757866144180298, -0.8127994537353516, -0.5822321176528931)),
                           soy.atoms.Position((0.3474087119102478, 0.08671217411756516)), soy.atoms.Vector((0, 0, 0)))
    v35 = soy.atoms.Vertex(soy.atoms.Position((0.7071067690849304, -0.7071067690849304, -1.0)),
                           soy.atoms.Vector((0.5688039660453796, -0.5688039660453796, -0.5940427780151367)),
                           soy.atoms.Position((0.3535991907119751, 0.07528285682201385)), soy.atoms.Vector((0, 0, 0)))
    v36 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, -1.0)), soy.atoms.Vector((0.0, 0.0, -1.0)),
                           soy.atoms.Position((0.3432457447052002, 0.06220778077840805)), soy.atoms.Vector((0, 0, 0)))
    v37 = soy.atoms.Vertex(soy.atoms.Position((-8.742277657347586e-08, -1.0, -1.0)),
                           soy.atoms.Vector((-0.01757866144180298, -0.8127994537353516, -0.5822321176528931)),
                           soy.atoms.Position((0.3474087119102478, 0.08671217411756516)), soy.atoms.Vector((0, 0, 0)))
    v38 = soy.atoms.Vertex(soy.atoms.Position((-0.70710688829422, -0.7071066498756409, -1.0)),
                           soy.atoms.Vector((-0.5838496088981628, -0.5838496088981628, -0.5641041398048401)),
                           soy.atoms.Position((0.3408326506614685, 0.08900528401136398)), soy.atoms.Vector((0, 0, 0)))
    v39 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, 3.9740030765533447)),
                           soy.atoms.Vector((-0.029663991183042526, 0.012268440797924995, 0.999481201171875)),
                           soy.atoms.Position((0.34243789315223694, 0.9999517202377319)), soy.atoms.Vector((0, 0, 0)))
    v40 = soy.atoms.Vertex(soy.atoms.Position((-0.4527122974395752, -0.45271211862564087, 1.052250623703003)),
                           soy.atoms.Vector((-0.774407148361206, -0.6091799736022949, 0.17069002985954285)),
                           soy.atoms.Position((0.32837894558906555, 0.21060481667518616)), soy.atoms.Vector((0, 0, 0)))
    v41 = soy.atoms.Vertex(soy.atoms.Position((-5.716119844123568e-08, -0.8001823425292969, 1.0836149454116821)),
                           soy.atoms.Vector((-0.12988677620887756, -0.9755851626396179, 0.17694631218910217)),
                           soy.atoms.Position((0.3328333795070648, 0.2146204710006714)), soy.atoms.Vector((0, 0, 0)))
    v42 = soy.atoms.Vertex(soy.atoms.Position((-8.742277657347586e-08, -1.0, -1.0)),
                           soy.atoms.Vector((-0.01757866144180298, -0.8127994537353516, -0.5822321176528931)),
                           soy.atoms.Position((0.3474087119102478, 0.08671217411756516)), soy.atoms.Vector((0, 0, 0)))
    v43 = soy.atoms.Vertex(soy.atoms.Position((-0.4527122974395752, -0.45271211862564087, 1.052250623703003)),
                           soy.atoms.Vector((-0.774407148361206, -0.6091799736022949, 0.17069002985954285)),
                           soy.atoms.Position((0.32837894558906555, 0.21060481667518616)), soy.atoms.Vector((0, 0, 0)))
    v44 = soy.atoms.Vertex(soy.atoms.Position((-0.70710688829422, -0.7071066498756409, -1.0)),
                           soy.atoms.Vector((-0.5838496088981628, -0.5838496088981628, -0.5641041398048401)),
                           soy.atoms.Position((0.3408326506614685, 0.08900528401136398)), soy.atoms.Vector((0, 0, 0)))
    v45 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, -1.0)), soy.atoms.Vector((0.0, 0.0, -1.0)),
                           soy.atoms.Position((0.3432457447052002, 0.06220778077840805)), soy.atoms.Vector((0, 0, 0)))
    v46 = soy.atoms.Vertex(soy.atoms.Position((-0.70710688829422, -0.7071066498756409, -1.0)),
                           soy.atoms.Vector((-0.5838496088981628, -0.5838496088981628, -0.5641041398048401)),
                           soy.atoms.Position((0.3408326506614685, 0.08900528401136398)), soy.atoms.Vector((0, 0, 0)))
    v47 = soy.atoms.Vertex(soy.atoms.Position((-1.0, 1.1924880638503055e-08, -1.0)),
                           soy.atoms.Vector((-0.825678288936615, 0.0, -0.5641041398048401)),
                           soy.atoms.Position((0.3358173370361328, 0.0829748809337616)), soy.atoms.Vector((0, 0, 0)))
    v48 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, 3.9740030765533447)),
                           soy.atoms.Vector((-0.029663991183042526, 0.012268440797924995, 0.999481201171875)),
                           soy.atoms.Position((0.34243789315223694, 0.9999517202377319)), soy.atoms.Vector((0, 0, 0)))
    v49 = soy.atoms.Vertex(soy.atoms.Position((-0.640231728553772, 7.634686660651369e-09, 1.052250623703003)),
                           soy.atoms.Vector((-0.9812921285629272, 0.0, 0.19241920113563538)),
                           soy.atoms.Position((0.33578529953956604, 0.20685933530330658)), soy.atoms.Vector((0, 0, 0)))
    v50 = soy.atoms.Vertex(soy.atoms.Position((-0.4527122974395752, -0.45271211862564087, 1.052250623703003)),
                           soy.atoms.Vector((-0.774407148361206, -0.6091799736022949, 0.17069002985954285)),
                           soy.atoms.Position((0.32837894558906555, 0.21060481667518616)), soy.atoms.Vector((0, 0, 0)))
    v51 = soy.atoms.Vertex(soy.atoms.Position((-0.4527122974395752, -0.45271211862564087, 1.052250623703003)),
                           soy.atoms.Vector((-0.774407148361206, -0.6091799736022949, 0.17069002985954285)),
                           soy.atoms.Position((0.32837894558906555, 0.21060481667518616)), soy.atoms.Vector((0, 0, 0)))
    v52 = soy.atoms.Vertex(soy.atoms.Position((-1.0, 1.1924880638503055e-08, -1.0)),
                           soy.atoms.Vector((-0.825678288936615, 0.0, -0.5641041398048401)),
                           soy.atoms.Position((0.3358173370361328, 0.0829748809337616)), soy.atoms.Vector((0, 0, 0)))
    v53 = soy.atoms.Vertex(soy.atoms.Position((-0.70710688829422, -0.7071066498756409, -1.0)),
                           soy.atoms.Vector((-0.5838496088981628, -0.5838496088981628, -0.5641041398048401)),
                           soy.atoms.Position((0.3408326506614685, 0.08900528401136398)), soy.atoms.Vector((0, 0, 0)))
    v54 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, -1.0)), soy.atoms.Vector((0.0, 0.0, -1.0)),
                           soy.atoms.Position((0.3432457447052002, 0.06220778077840805)), soy.atoms.Vector((0, 0, 0)))
    v55 = soy.atoms.Vertex(soy.atoms.Position((-1.0, 1.1924880638503055e-08, -1.0)),
                           soy.atoms.Vector((-0.825678288936615, 0.0, -0.5641041398048401)),
                           soy.atoms.Position((0.3358173370361328, 0.0829748809337616)), soy.atoms.Vector((0, 0, 0)))
    v56 = soy.atoms.Vertex(soy.atoms.Position((-0.70710688829422, 0.7071066498756409, -1.0)),
                           soy.atoms.Vector((-0.5838496088981628, 0.5838496088981628, -0.5641041398048401)),
                           soy.atoms.Position((0.33285850286483765, 0.06971260160207748)), soy.atoms.Vector((0, 0, 0)))
    v57 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, 3.9740030765533447)),
                           soy.atoms.Vector((-0.029663991183042526, 0.012268440797924995, 0.999481201171875)),
                           soy.atoms.Position((0.34243789315223694, 0.9999517202377319)), soy.atoms.Vector((0, 0, 0)))
    v58 = soy.atoms.Vertex(soy.atoms.Position((-0.4527122974395752, 0.45271211862564087, 1.052250623703003)),
                           soy.atoms.Vector((-0.6938688158988953, 0.6938688158988953, 0.19241920113563538)),
                           soy.atoms.Position((0.3441375494003296, 0.2036278396844864)), soy.atoms.Vector((0, 0, 0)))
    v59 = soy.atoms.Vertex(soy.atoms.Position((-0.640231728553772, 7.634686660651369e-09, 1.052250623703003)),
                           soy.atoms.Vector((-0.9812921285629272, 0.0, 0.19241920113563538)),
                           soy.atoms.Position((0.33578529953956604, 0.20685933530330658)), soy.atoms.Vector((0, 0, 0)))
    v60 = soy.atoms.Vertex(soy.atoms.Position((-0.640231728553772, 7.634686660651369e-09, 1.052250623703003)),
                           soy.atoms.Vector((-0.9812921285629272, 0.0, 0.19241920113563538)),
                           soy.atoms.Position((0.33578529953956604, 0.20685933530330658)), soy.atoms.Vector((0, 0, 0)))
    v61 = soy.atoms.Vertex(soy.atoms.Position((-0.70710688829422, 0.7071066498756409, -1.0)),
                           soy.atoms.Vector((-0.5838496088981628, 0.5838496088981628, -0.5641041398048401)),
                           soy.atoms.Position((0.33285850286483765, 0.06971260160207748)), soy.atoms.Vector((0, 0, 0)))
    v62 = soy.atoms.Vertex(soy.atoms.Position((-1.0, 1.1924880638503055e-08, -1.0)),
                           soy.atoms.Vector((-0.825678288936615, 0.0, -0.5641041398048401)),
                           soy.atoms.Position((0.3358173370361328, 0.0829748809337616)), soy.atoms.Vector((0, 0, 0)))
    v63 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, -1.0)), soy.atoms.Vector((0.0, 0.0, -1.0)),
                           soy.atoms.Position((0.3432457447052002, 0.06220778077840805)), soy.atoms.Vector((0, 0, 0)))
    v64 = soy.atoms.Vertex(soy.atoms.Position((-0.70710688829422, 0.7071066498756409, -1.0)),
                           soy.atoms.Vector((-0.5838496088981628, 0.5838496088981628, -0.5641041398048401)),
                           soy.atoms.Position((0.33285850286483765, 0.06971260160207748)), soy.atoms.Vector((0, 0, 0)))
    v65 = soy.atoms.Vertex(soy.atoms.Position((0.0, 1.0, -1.0)),
                           soy.atoms.Vector((0.0, 0.825678288936615, -0.5641041398048401)),
                           soy.atoms.Position((0.3339645564556122, 0.04476860910654068)), soy.atoms.Vector((0, 0, 0)))
    v66 = soy.atoms.Vertex(soy.atoms.Position((0.0, 0.0, 3.9740030765533447)),
                           soy.atoms.Vector((-0.029663991183042526, 0.012268440797924995, 0.999481201171875)),
                           soy.atoms.Position((0.34243789315223694, 0.9999517202377319)), soy.atoms.Vector((0, 0, 0)))
    v67 = soy.atoms.Vertex(soy.atoms.Position((-1.190366027969958e-09, 0.640231728553772, 1.052250623703003)),
                           soy.atoms.Vector((-0.11682485044002533, 0.9783623814582825, 0.17069002985954285)),
                           soy.atoms.Position((0.35524094104766846, 0.20218642055988312)), soy.atoms.Vector((0, 0, 0)))
    v68 = soy.atoms.Vertex(soy.atoms.Position((-0.4527122974395752, 0.45271211862564087, 1.052250623703003)),
                           soy.atoms.Vector((-0.6938688158988953, 0.6938688158988953, 0.19241920113563538)),
                           soy.atoms.Position((0.3441375494003296, 0.2036278396844864)), soy.atoms.Vector((0, 0, 0)))
    v69 = soy.atoms.Vertex(soy.atoms.Position((-0.4527122974395752, 0.45271211862564087, 1.052250623703003)),
                           soy.atoms.Vector((-0.6938688158988953, 0.6938688158988953, 0.19241920113563538)),
                           soy.atoms.Position((0.3441375494003296, 0.2036278396844864)), soy.atoms.Vector((0, 0, 0)))
    v70 = soy.atoms.Vertex(soy.atoms.Position((0.0, 1.0, -1.0)),
                           soy.atoms.Vector((0.0, 0.825678288936615, -0.5641041398048401)),
                           soy.atoms.Position((0.3339645564556122, 0.04476860910654068)), soy.atoms.Vector((0, 0, 0)))
    v71 = soy.atoms.Vertex(soy.atoms.Position((-0.70710688829422, 0.7071066498756409, -1.0)),
                           soy.atoms.Vector((-0.5838496088981628, 0.5838496088981628, -0.5641041398048401)),
                           soy.atoms.Position((0.33285850286483765, 0.06971260160207748)), soy.atoms.Vector((0, 0, 0)))
    v72 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.5754127502441406, 3.803668975830078)),
                           soy.atoms.Vector((0.0, 1.0, 0.0)),
                           soy.atoms.Position((0.2142072468996048, 0.056988880038261414)), soy.atoms.Vector((0, 0, 0)))
    v73 = soy.atoms.Vertex(soy.atoms.Position((0.7740284204483032, 0.7853434085845947, 2.686342239379883)),
                           soy.atoms.Vector((0.5293435454368591, 0.5293435454368591, -0.6629840731620789)),
                           soy.atoms.Position((0.2204035222530365, 0.09558363258838654)), soy.atoms.Vector((0, 0, 0)))
    v74 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.1126008033752441, 2.686342239379883)),
                           soy.atoms.Vector((0.0, 0.748588502407074, -0.6629840731620789)),
                           soy.atoms.Position((0.20222534239292145, 0.08384476602077484)), soy.atoms.Vector((0, 0, 0)))
    v75 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.1126008033752441, 4.92099666595459)),
                           soy.atoms.Vector((0.0, 0.748588502407074, 0.6629840731620789)),
                           soy.atoms.Position((0.059787098318338394, 0.2463478147983551)), soy.atoms.Vector((0, 0, 0)))
    v76 = soy.atoms.Vertex(soy.atoms.Position((-0.016041383147239685, -0.004726370330899954, 5.383808135986328)),
                           soy.atoms.Vector((0.0, 0.0, 1.0)),
                           soy.atoms.Position((0.04229000210762024, 0.20410607755184174)), soy.atoms.Vector((0, 0, 0)))
    v77 = soy.atoms.Vertex(soy.atoms.Position((0.7740284204483032, 0.7853434085845947, 4.92099666595459)),
                           soy.atoms.Vector((0.5293435454368591, 0.5293435454368591, 0.6629840731620789)),
                           soy.atoms.Position((0.0845317393541336, 0.22160319983959198)), soy.atoms.Vector((0, 0, 0)))
    v78 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -0.0047261822037398815, 2.223529815673828)),
                           soy.atoms.Vector((0.0, 0.0, -1.0)),
                           soy.atoms.Position((0.18258970975875854, 0.12963975965976715)), soy.atoms.Vector((0, 0, 0)))
    v79 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.1126008033752441, 2.686342239379883)),
                           soy.atoms.Vector((0.0, 0.748588502407074, -0.6629840731620789)),
                           soy.atoms.Position((0.20222534239292145, 0.08384476602077484)), soy.atoms.Vector((0, 0, 0)))
    v80 = soy.atoms.Vertex(soy.atoms.Position((0.7740284204483032, 0.7853434085845947, 2.686342239379883)),
                           soy.atoms.Vector((0.5293435454368591, 0.5293435454368591, -0.6629840731620789)),
                           soy.atoms.Position((0.2204035222530365, 0.09558363258838654)), soy.atoms.Vector((0, 0, 0)))
    v81 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.1126008033752441, 4.92099666595459)),
                           soy.atoms.Vector((0.0, 0.748588502407074, 0.6629840731620789)),
                           soy.atoms.Position((0.23088151216506958, 0.012395179830491543)), soy.atoms.Vector((0, 0, 0)))
    v82 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, 1.1126011610031128, 3.803668975830078)),
                           soy.atoms.Vector((0.7070833444595337, 0.7070833444595337, 0.0)),
                           soy.atoms.Position((0.24132287502288818, 0.07176600396633148)), soy.atoms.Vector((0, 0, 0)))
    v83 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.5754127502441406, 3.803668975830078)),
                           soy.atoms.Vector((0.0, 1.0, 0.0)),
                           soy.atoms.Position((0.2142072468996048, 0.056988880038261414)), soy.atoms.Vector((0, 0, 0)))
    v84 = soy.atoms.Vertex(soy.atoms.Position((0.7740284204483032, 0.7853434085845947, 4.92099666595459)),
                           soy.atoms.Vector((0.5293435454368591, 0.5293435454368591, 0.6629840731620789)),
                           soy.atoms.Position((0.0845317393541336, 0.22160319983959198)), soy.atoms.Vector((0, 0, 0)))
    v85 = soy.atoms.Vertex(soy.atoms.Position((-0.016041383147239685, -0.004726370330899954, 5.383808135986328)),
                           soy.atoms.Vector((0.0, 0.0, 1.0)),
                           soy.atoms.Position((0.04229000210762024, 0.20410607755184174)), soy.atoms.Vector((0, 0, 0)))
    v86 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -0.0047261822037398815, 4.92099666595459)),
                           soy.atoms.Vector((0.748588502407074, 0.0, 0.6629840731620789)),
                           soy.atoms.Position((0.0845317617058754, 0.18660897016525269)), soy.atoms.Vector((0, 0, 0)))
    v87 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -0.0047261822037398815, 2.223529815673828)),
                           soy.atoms.Vector((0.0, 0.0, -1.0)),
                           soy.atoms.Position((0.18258970975875854, 0.12963975965976715)), soy.atoms.Vector((0, 0, 0)))
    v88 = soy.atoms.Vertex(soy.atoms.Position((0.7740284204483032, 0.7853434085845947, 2.686342239379883)),
                           soy.atoms.Vector((0.5293435454368591, 0.5293435454368591, -0.6629840731620789)),
                           soy.atoms.Position((0.2204035222530365, 0.09558363258838654)), soy.atoms.Vector((0, 0, 0)))
    v89 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -0.0047261822037398815, 2.686342239379883)),
                           soy.atoms.Vector((0.748588502407074, 0.0, -0.6629840731620789)),
                           soy.atoms.Position((0.2357088029384613, 0.11679655313491821)), soy.atoms.Vector((0, 0, 0)))
    v90 = soy.atoms.Vertex(soy.atoms.Position((0.7740284204483032, 0.7853434085845947, 4.92099666595459)),
                           soy.atoms.Vector((0.5293435454368591, 0.5293435454368591, 0.6629840731620789)),
                           soy.atoms.Position((0.2664477527141571, 0.03352435678243637)), soy.atoms.Vector((0, 0, 0)))
    v91 = soy.atoms.Vertex(soy.atoms.Position((1.5640983581542969, -0.0047261822037398815, 3.803668975830078)),
                           soy.atoms.Vector((1.0, 0.0, 0.0)),
                           soy.atoms.Position((0.26693272590637207, 0.0941304862499237)), soy.atoms.Vector((0, 0, 0)))
    v92 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, 1.1126011610031128, 3.803668975830078)),
                           soy.atoms.Vector((0.7070833444595337, 0.7070833444595337, 0.0)),
                           soy.atoms.Position((0.24132287502288818, 0.07176600396633148)), soy.atoms.Vector((0, 0, 0)))
    v93 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, 1.1126011610031128, 3.803668975830078)),
                           soy.atoms.Vector((0.7070833444595337, 0.7070833444595337, 0.0)),
                           soy.atoms.Position((0.24132287502288818, 0.07176600396633148)), soy.atoms.Vector((0, 0, 0)))
    v94 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -0.0047261822037398815, 2.686342239379883)),
                           soy.atoms.Vector((0.748588502407074, 0.0, -0.6629840731620789)),
                           soy.atoms.Position((0.2357088029384613, 0.11679655313491821)), soy.atoms.Vector((0, 0, 0)))
    v95 = soy.atoms.Vertex(soy.atoms.Position((0.7740284204483032, 0.7853434085845947, 2.686342239379883)),
                           soy.atoms.Vector((0.5293435454368591, 0.5293435454368591, -0.6629840731620789)),
                           soy.atoms.Position((0.2204035222530365, 0.09558363258838654)), soy.atoms.Vector((0, 0, 0)))
    v96 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -0.0047261822037398815, 2.686342239379883)),
                           soy.atoms.Vector((0.748588502407074, 0.0, -0.6629840731620789)),
                           soy.atoms.Position((0.2357088029384613, 0.11679655313491821)), soy.atoms.Vector((0, 0, 0)))
    v97 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -1.1220535039901733, 3.803668975830078)),
                           soy.atoms.Vector((0.7070833444595337, -0.7070833444595337, 0.0)),
                           soy.atoms.Position((0.2988373339176178, 0.13461822271347046)), soy.atoms.Vector((0, 0, 0)))
    v98 = soy.atoms.Vertex(soy.atoms.Position((0.7740283012390137, -0.7947957515716553, 2.686342239379883)),
                           soy.atoms.Vector((0.5293435454368591, -0.5293435454368591, -0.6629840731620789)),
                           soy.atoms.Position((0.24257539212703705, 0.15173116326332092)), soy.atoms.Vector((0, 0, 0)))
    v99 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -0.0047261822037398815, 4.92099666595459)),
                           soy.atoms.Vector((0.748588502407074, 0.0, 0.6629840731620789)),
                           soy.atoms.Position((0.0845317617058754, 0.18660897016525269)), soy.atoms.Vector((0, 0, 0)))
    v100 = soy.atoms.Vertex(soy.atoms.Position((-0.016041383147239685, -0.004726370330899954, 5.383808135986328)),
                            soy.atoms.Vector((0.0, 0.0, 1.0)),
                            soy.atoms.Position((0.04229000210762024, 0.20410607755184174)), soy.atoms.Vector((0, 0, 0)))
    v101 = soy.atoms.Vertex(soy.atoms.Position((0.7740283012390137, -0.7947957515716553, 4.92099666595459)),
                            soy.atoms.Vector((0.5293435454368591, -0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.059787098318338394, 0.16186435520648956)),
                            soy.atoms.Vector((0, 0, 0)))
    v102 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -0.0047261822037398815, 2.223529815673828)),
                            soy.atoms.Vector((0.0, 0.0, -1.0)),
                            soy.atoms.Position((0.18258970975875854, 0.12963975965976715)), soy.atoms.Vector((0, 0, 0)))
    v103 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -0.0047261822037398815, 2.686342239379883)),
                            soy.atoms.Vector((0.748588502407074, 0.0, -0.6629840731620789)),
                            soy.atoms.Position((0.2357088029384613, 0.11679655313491821)), soy.atoms.Vector((0, 0, 0)))
    v104 = soy.atoms.Vertex(soy.atoms.Position((0.7740283012390137, -0.7947957515716553, 2.686342239379883)),
                            soy.atoms.Vector((0.5293435454368591, -0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.24257539212703705, 0.15173116326332092)), soy.atoms.Vector((0, 0, 0)))
    v105 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -0.0047261822037398815, 4.92099666595459)),
                            soy.atoms.Vector((0.748588502407074, 0.0, 0.6629840731620789)),
                            soy.atoms.Position((0.2965916693210602, 0.05921382084488869)), soy.atoms.Vector((0, 0, 0)))
    v106 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -1.1220535039901733, 3.803668975830078)),
                            soy.atoms.Vector((0.7070833444595337, -0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.2988373339176178, 0.13461822271347046)), soy.atoms.Vector((0, 0, 0)))
    v107 = soy.atoms.Vertex(soy.atoms.Position((1.5640983581542969, -0.0047261822037398815, 3.803668975830078)),
                            soy.atoms.Vector((1.0, 0.0, 0.0)),
                            soy.atoms.Position((0.26693272590637207, 0.0941304862499237)), soy.atoms.Vector((0, 0, 0)))
    v108 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -1.1220535039901733, 3.803668975830078)),
                            soy.atoms.Vector((0.7070833444595337, -0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.05025875195860863, 0.1478371024131775)), soy.atoms.Vector((0, 0, 0)))
    v109 = soy.atoms.Vertex(soy.atoms.Position((-0.016041234135627747, -1.1220531463623047, 2.686342239379883)),
                            soy.atoms.Vector((0.0, -0.748588502407074, -0.6629840731620789)),
                            soy.atoms.Position((0.12589187920093536, 0.1205931082367897)), soy.atoms.Vector((0, 0, 0)))
    v110 = soy.atoms.Vertex(soy.atoms.Position((0.7740283012390137, -0.7947957515716553, 2.686342239379883)),
                            soy.atoms.Vector((0.5293435454368591, -0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.12091799825429916, 0.16176782548427582)), soy.atoms.Vector((0, 0, 0)))
    v111 = soy.atoms.Vertex(soy.atoms.Position((0.7740283012390137, -0.7947957515716553, 4.92099666595459)),
                            soy.atoms.Vector((0.5293435454368591, -0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.059787098318338394, 0.16186435520648956)),
                            soy.atoms.Vector((0, 0, 0)))
    v112 = soy.atoms.Vertex(soy.atoms.Position((-0.016041383147239685, -0.004726370330899954, 5.383808135986328)),
                            soy.atoms.Vector((0.0, 0.0, 1.0)),
                            soy.atoms.Position((0.04229000210762024, 0.20410607755184174)), soy.atoms.Vector((0, 0, 0)))
    v113 = soy.atoms.Vertex(soy.atoms.Position((-0.016041234135627747, -1.1220531463623047, 4.92099666595459)),
                            soy.atoms.Vector((0.0, -0.748588502407074, 0.6629840731620789)),
                            soy.atoms.Position((0.024792909622192383, 0.16186437010765076)),
                            soy.atoms.Vector((0, 0, 0)))
    v114 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -0.0047261822037398815, 2.223529815673828)),
                            soy.atoms.Vector((0.0, 0.0, -1.0)),
                            soy.atoms.Position((0.18258970975875854, 0.12963975965976715)), soy.atoms.Vector((0, 0, 0)))
    v115 = soy.atoms.Vertex(soy.atoms.Position((0.7740283012390137, -0.7947957515716553, 2.686342239379883)),
                            soy.atoms.Vector((0.5293435454368591, -0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.12091799825429916, 0.16176782548427582)), soy.atoms.Vector((0, 0, 0)))
    v116 = soy.atoms.Vertex(soy.atoms.Position((-0.016041234135627747, -1.1220531463623047, 2.686342239379883)),
                            soy.atoms.Vector((0.0, -0.748588502407074, -0.6629840731620789)),
                            soy.atoms.Position((0.12589187920093536, 0.1205931082367897)), soy.atoms.Vector((0, 0, 0)))
    v117 = soy.atoms.Vertex(soy.atoms.Position((0.7740283012390137, -0.7947957515716553, 4.92099666595459)),
                            soy.atoms.Vector((0.5293435454368591, -0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((4.8266916564898565e-05, 0.07856482267379761)),
                            soy.atoms.Vector((0, 0, 0)))
    v118 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -1.5848652124404907, 3.803668975830078)),
                            soy.atoms.Vector((0.0, -1.0, 0.0)),
                            soy.atoms.Position((0.0892295092344284, 0.09508739411830902)), soy.atoms.Vector((0, 0, 0)))
    v119 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -1.1220535039901733, 3.803668975830078)),
                            soy.atoms.Vector((0.7070833444595337, -0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.05025875195860863, 0.1478371024131775)), soy.atoms.Vector((0, 0, 0)))
    v120 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -1.5848652124404907, 3.803668975830078)),
                            soy.atoms.Vector((0.0, -1.0, 0.0)),
                            soy.atoms.Position((0.0892295092344284, 0.09508739411830902)), soy.atoms.Vector((0, 0, 0)))
    v121 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, -0.7947955131530762, 2.686342239379883)),
                            soy.atoms.Vector((-0.5293435454368591, -0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.14164607226848602, 0.09647044539451599)), soy.atoms.Vector((0, 0, 0)))
    v122 = soy.atoms.Vertex(soy.atoms.Position((-0.016041234135627747, -1.1220531463623047, 2.686342239379883)),
                            soy.atoms.Vector((0.0, -0.748588502407074, -0.6629840731620789)),
                            soy.atoms.Position((0.12589187920093536, 0.1205931082367897)), soy.atoms.Vector((0, 0, 0)))
    v123 = soy.atoms.Vertex(soy.atoms.Position((-0.016041234135627747, -1.1220531463623047, 4.92099666595459)),
                            soy.atoms.Vector((0.0, -0.748588502407074, 0.6629840731620789)),
                            soy.atoms.Position((0.024792909622192383, 0.16186437010765076)),
                            soy.atoms.Vector((0, 0, 0)))
    v124 = soy.atoms.Vertex(soy.atoms.Position((-0.016041383147239685, -0.004726370330899954, 5.383808135986328)),
                            soy.atoms.Vector((0.0, 0.0, 1.0)),
                            soy.atoms.Position((0.04229000210762024, 0.20410607755184174)), soy.atoms.Vector((0, 0, 0)))
    v125 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, -0.7947955131530762, 4.92099666595459)),
                            soy.atoms.Vector((-0.5293435454368591, -0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((4.827290467801504e-05, 0.18660897016525269)),
                            soy.atoms.Vector((0, 0, 0)))
    v126 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -0.0047261822037398815, 2.223529815673828)),
                            soy.atoms.Vector((0.0, 0.0, -1.0)),
                            soy.atoms.Position((0.18258970975875854, 0.12963975965976715)), soy.atoms.Vector((0, 0, 0)))
    v127 = soy.atoms.Vertex(soy.atoms.Position((-0.016041234135627747, -1.1220531463623047, 2.686342239379883)),
                            soy.atoms.Vector((0.0, -0.748588502407074, -0.6629840731620789)),
                            soy.atoms.Position((0.12589187920093536, 0.1205931082367897)), soy.atoms.Vector((0, 0, 0)))
    v128 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, -0.7947955131530762, 2.686342239379883)),
                            soy.atoms.Vector((-0.5293435454368591, -0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.14164607226848602, 0.09647044539451599)), soy.atoms.Vector((0, 0, 0)))
    v129 = soy.atoms.Vertex(soy.atoms.Position((-0.016041234135627747, -1.1220531463623047, 4.92099666595459)),
                            soy.atoms.Vector((0.0, -0.748588502407074, 0.6629840731620789)),
                            soy.atoms.Position((0.046560660004615784, 0.04192383959889412)),
                            soy.atoms.Vector((0, 0, 0)))
    v130 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -1.1220531463623047, 3.803668975830078)),
                            soy.atoms.Vector((-0.7070833444595337, -0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.12132260203361511, 0.06741581857204437)), soy.atoms.Vector((0, 0, 0)))
    v131 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -1.5848652124404907, 3.803668975830078)),
                            soy.atoms.Vector((0.0, -1.0, 0.0)),
                            soy.atoms.Position((0.0892295092344284, 0.09508739411830902)), soy.atoms.Vector((0, 0, 0)))
    v132 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -1.1220531463623047, 3.803668975830078)),
                            soy.atoms.Vector((-0.7070833444595337, -0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.12132260203361511, 0.06741581857204437)), soy.atoms.Vector((0, 0, 0)))
    v133 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -0.0047261822037398815, 2.686342239379883)),
                            soy.atoms.Vector((-0.748588502407074, 0.0, -0.6629840731620789)),
                            soy.atoms.Position((0.16129980981349945, 0.0836634412407875)), soy.atoms.Vector((0, 0, 0)))
    v134 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, -0.7947955131530762, 2.686342239379883)),
                            soy.atoms.Vector((-0.5293435454368591, -0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.14164607226848602, 0.09647044539451599)), soy.atoms.Vector((0, 0, 0)))
    v135 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, -0.7947955131530762, 4.92099666595459)),
                            soy.atoms.Vector((-0.5293435454368591, -0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((4.827290467801504e-05, 0.18660897016525269)),
                            soy.atoms.Vector((0, 0, 0)))
    v136 = soy.atoms.Vertex(soy.atoms.Position((-0.016041383147239685, -0.004726370330899954, 5.383808135986328)),
                            soy.atoms.Vector((0.0, 0.0, 1.0)),
                            soy.atoms.Position((0.04229000210762024, 0.20410607755184174)), soy.atoms.Vector((0, 0, 0)))
    v137 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -0.0047261822037398815, 4.92099666595459)),
                            soy.atoms.Vector((-0.748588502407074, 0.0, 0.6629840731620789)),
                            soy.atoms.Position((4.8266916564898565e-05, 0.2216031700372696)),
                            soy.atoms.Vector((0, 0, 0)))
    v138 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -0.0047261822037398815, 2.223529815673828)),
                            soy.atoms.Vector((0.0, 0.0, -1.0)),
                            soy.atoms.Position((0.18258970975875854, 0.12963975965976715)), soy.atoms.Vector((0, 0, 0)))
    v139 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, -0.7947955131530762, 2.686342239379883)),
                            soy.atoms.Vector((-0.5293435454368591, -0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.14164607226848602, 0.09647044539451599)), soy.atoms.Vector((0, 0, 0)))
    v140 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -0.0047261822037398815, 2.686342239379883)),
                            soy.atoms.Vector((-0.748588502407074, 0.0, -0.6629840731620789)),
                            soy.atoms.Position((0.16129980981349945, 0.0836634412407875)), soy.atoms.Vector((0, 0, 0)))
    v141 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, -0.7947955131530762, 4.92099666595459)),
                            soy.atoms.Vector((-0.5293435454368591, -0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.09350647032260895, 0.01426364853978157)), soy.atoms.Vector((0, 0, 0)))
    v142 = soy.atoms.Vertex(soy.atoms.Position((-1.5961802005767822, -0.0047261822037398815, 3.803668975830078)),
                            soy.atoms.Vector((-1.0, 0.0, 0.0)),
                            soy.atoms.Position((0.15326373279094696, 0.05347014591097832)), soy.atoms.Vector((0, 0, 0)))
    v143 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -1.1220531463623047, 3.803668975830078)),
                            soy.atoms.Vector((-0.7070833444595337, -0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.12132260203361511, 0.06741581857204437)), soy.atoms.Vector((0, 0, 0)))
    v144 = soy.atoms.Vertex(soy.atoms.Position((-1.5961802005767822, -0.0047261822037398815, 3.803668975830078)),
                            soy.atoms.Vector((-1.0, 0.0, 0.0)),
                            soy.atoms.Position((0.15326373279094696, 0.05347014591097832)), soy.atoms.Vector((0, 0, 0)))
    v145 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, 0.7853432893753052, 2.686342239379883)),
                            soy.atoms.Vector((-0.5293435454368591, 0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.18209107220172882, 0.07979989051818848)), soy.atoms.Vector((0, 0, 0)))
    v146 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -0.0047261822037398815, 2.686342239379883)),
                            soy.atoms.Vector((-0.748588502407074, 0.0, -0.6629840731620789)),
                            soy.atoms.Position((0.16129980981349945, 0.0836634412407875)), soy.atoms.Vector((0, 0, 0)))
    v147 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -0.0047261822037398815, 4.92099666595459)),
                            soy.atoms.Vector((-0.748588502407074, 0.0, 0.6629840731620789)),
                            soy.atoms.Position((4.8266916564898565e-05, 0.2216031700372696)),
                            soy.atoms.Vector((0, 0, 0)))
    v148 = soy.atoms.Vertex(soy.atoms.Position((-0.016041383147239685, -0.004726370330899954, 5.383808135986328)),
                            soy.atoms.Vector((0.0, 0.0, 1.0)),
                            soy.atoms.Position((0.04229000210762024, 0.20410607755184174)), soy.atoms.Vector((0, 0, 0)))
    v149 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, 0.7853432893753052, 4.92099666595459)),
                            soy.atoms.Vector((-0.5293435454368591, 0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.024792887270450592, 0.2463478446006775)), soy.atoms.Vector((0, 0, 0)))
    v150 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -0.0047261822037398815, 2.223529815673828)),
                            soy.atoms.Vector((0.0, 0.0, -1.0)),
                            soy.atoms.Position((0.18258970975875854, 0.12963975965976715)), soy.atoms.Vector((0, 0, 0)))
    v151 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -0.0047261822037398815, 2.686342239379883)),
                            soy.atoms.Vector((-0.748588502407074, 0.0, -0.6629840731620789)),
                            soy.atoms.Position((0.16129980981349945, 0.0836634412407875)), soy.atoms.Vector((0, 0, 0)))
    v152 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, 0.7853432893753052, 2.686342239379883)),
                            soy.atoms.Vector((-0.5293435454368591, 0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.18209107220172882, 0.07979989051818848)), soy.atoms.Vector((0, 0, 0)))
    v153 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -0.0047261822037398815, 4.92099666595459)),
                            soy.atoms.Vector((-0.748588502407074, 0.0, 0.6629840731620789)),
                            soy.atoms.Position((0.1418866664171219, 4.829086901736446e-05)),
                            soy.atoms.Vector((0, 0, 0)))
    v154 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, 1.1126008033752441, 3.803668975830078)),
                            soy.atoms.Vector((-0.7070833444595337, 0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.18463541567325592, 0.0504889078438282)), soy.atoms.Vector((0, 0, 0)))
    v155 = soy.atoms.Vertex(soy.atoms.Position((-1.5961802005767822, -0.0047261822037398815, 3.803668975830078)),
                            soy.atoms.Vector((-1.0, 0.0, 0.0)),
                            soy.atoms.Position((0.15326373279094696, 0.05347014591097832)), soy.atoms.Vector((0, 0, 0)))
    v156 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, 0.7853432893753052, 4.92099666595459)),
                            soy.atoms.Vector((-0.5293435454368591, 0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.18878695368766785, 4.8266916564898565e-05)),
                            soy.atoms.Vector((0, 0, 0)))
    v157 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.5754127502441406, 3.803668975830078)),
                            soy.atoms.Vector((0.0, 1.0, 0.0)),
                            soy.atoms.Position((0.2142072468996048, 0.056988880038261414)), soy.atoms.Vector((0, 0, 0)))
    v158 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, 1.1126008033752441, 3.803668975830078)),
                            soy.atoms.Vector((-0.7070833444595337, 0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.18463541567325592, 0.0504889078438282)), soy.atoms.Vector((0, 0, 0)))
    v159 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, 1.1126008033752441, 3.803668975830078)),
                            soy.atoms.Vector((-0.7070833444595337, 0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.18463541567325592, 0.0504889078438282)), soy.atoms.Vector((0, 0, 0)))
    v160 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.1126008033752441, 2.686342239379883)),
                            soy.atoms.Vector((0.0, 0.748588502407074, -0.6629840731620789)),
                            soy.atoms.Position((0.20222534239292145, 0.08384476602077484)), soy.atoms.Vector((0, 0, 0)))
    v161 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, 0.7853432893753052, 2.686342239379883)),
                            soy.atoms.Vector((-0.5293435454368591, 0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.18209107220172882, 0.07979989051818848)), soy.atoms.Vector((0, 0, 0)))
    v162 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, 0.7853432893753052, 4.92099666595459)),
                            soy.atoms.Vector((-0.5293435454368591, 0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.024792887270450592, 0.2463478446006775)), soy.atoms.Vector((0, 0, 0)))
    v163 = soy.atoms.Vertex(soy.atoms.Position((-0.016041383147239685, -0.004726370330899954, 5.383808135986328)),
                            soy.atoms.Vector((0.0, 0.0, 1.0)),
                            soy.atoms.Position((0.04229000210762024, 0.20410607755184174)), soy.atoms.Vector((0, 0, 0)))
    v164 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.1126008033752441, 4.92099666595459)),
                            soy.atoms.Vector((0.0, 0.748588502407074, 0.6629840731620789)),
                            soy.atoms.Position((0.059787098318338394, 0.2463478147983551)), soy.atoms.Vector((0, 0, 0)))
    v165 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -0.0047261822037398815, 2.223529815673828)),
                            soy.atoms.Vector((0.0, 0.0, -1.0)),
                            soy.atoms.Position((0.18258970975875854, 0.12963975965976715)), soy.atoms.Vector((0, 0, 0)))
    v166 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, 0.7853432893753052, 2.686342239379883)),
                            soy.atoms.Vector((-0.5293435454368591, 0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.18209107220172882, 0.07979989051818848)), soy.atoms.Vector((0, 0, 0)))
    v167 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.1126008033752441, 2.686342239379883)),
                            soy.atoms.Vector((0.0, 0.748588502407074, -0.6629840731620789)),
                            soy.atoms.Position((0.20222534239292145, 0.08384476602077484)), soy.atoms.Vector((0, 0, 0)))
    v168 = soy.atoms.Vertex(soy.atoms.Position((-1.190366027969958e-09, 0.640231728553772, 1.052250623703003)),
                            soy.atoms.Vector((-0.11682485044002533, 0.9783623814582825, 0.17069002985954285)),
                            soy.atoms.Position((0.35524094104766846, 0.20218642055988312)), soy.atoms.Vector((0, 0, 0)))
    v169 = soy.atoms.Vertex(soy.atoms.Position((0.5658128261566162, 0.5658128261566162, 1.0836149454116821)),
                            soy.atoms.Vector((0.5980101823806763, 0.7817010879516602, 0.17694631218910217)),
                            soy.atoms.Position((0.3565353453159332, 0.20357738435268402)), soy.atoms.Vector((0, 0, 0)))
    v170 = soy.atoms.Vertex(soy.atoms.Position((0.7071067690849304, 0.7071067690849304, -1.0)),
                            soy.atoms.Vector((0.5623035430908203, 0.5871761441230774, -0.5822321176528931)),
                            soy.atoms.Position((0.34557223320007324, 4.8266916564898565e-05)),
                            soy.atoms.Vector((0, 0, 0)))
    v171 = soy.atoms.Vertex(soy.atoms.Position((0.5658128261566162, 0.5658128261566162, 1.0836149454116821)),
                            soy.atoms.Vector((0.5980101823806763, 0.7817010879516602, 0.17694631218910217)),
                            soy.atoms.Position((0.3565353453159332, 0.20357738435268402)), soy.atoms.Vector((0, 0, 0)))
    v172 = soy.atoms.Vertex(soy.atoms.Position((0.8001823425292969, -2.7985416650722073e-08, 1.0836149454116821)),
                            soy.atoms.Vector((0.9839777946472168, 0.0, 0.17813654243946075)),
                            soy.atoms.Position((0.3503111004829407, 0.20928388833999634)), soy.atoms.Vector((0, 0, 0)))
    v173 = soy.atoms.Vertex(soy.atoms.Position((1.0, -4.371138828673793e-08, -1.0)),
                            soy.atoms.Vector((0.8044068813323975, 0.0, -0.5940427780151367)),
                            soy.atoms.Position((0.3559127449989319, 0.049157705157995224)), soy.atoms.Vector((0, 0, 0)))
    v174 = soy.atoms.Vertex(soy.atoms.Position((0.8001823425292969, -2.7985416650722073e-08, 1.0836149454116821)),
                            soy.atoms.Vector((0.9839777946472168, 0.0, 0.17813654243946075)),
                            soy.atoms.Position((0.3503111004829407, 0.20928388833999634)), soy.atoms.Vector((0, 0, 0)))
    v175 = soy.atoms.Vertex(soy.atoms.Position((0.5658128261566162, -0.5658128261566162, 1.0836149454116821)),
                            soy.atoms.Vector((0.695791482925415, -0.695791482925415, 0.17813654243946075)),
                            soy.atoms.Position((0.34243783354759216, 0.21376270055770874)), soy.atoms.Vector((0, 0, 0)))
    v176 = soy.atoms.Vertex(soy.atoms.Position((0.7071067690849304, -0.7071067690849304, -1.0)),
                            soy.atoms.Vector((0.5688039660453796, -0.5688039660453796, -0.5940427780151367)),
                            soy.atoms.Position((0.3535991907119751, 0.07528285682201385)), soy.atoms.Vector((0, 0, 0)))
    v177 = soy.atoms.Vertex(soy.atoms.Position((0.5658128261566162, -0.5658128261566162, 1.0836149454116821)),
                            soy.atoms.Vector((0.695791482925415, -0.695791482925415, 0.17813654243946075)),
                            soy.atoms.Position((0.34243783354759216, 0.21376270055770874)), soy.atoms.Vector((0, 0, 0)))
    v178 = soy.atoms.Vertex(soy.atoms.Position((-5.716119844123568e-08, -0.8001823425292969, 1.0836149454116821)),
                            soy.atoms.Vector((-0.12988677620887756, -0.9755851626396179, 0.17694631218910217)),
                            soy.atoms.Position((0.3328333795070648, 0.2146204710006714)), soy.atoms.Vector((0, 0, 0)))
    v179 = soy.atoms.Vertex(soy.atoms.Position((-8.742277657347586e-08, -1.0, -1.0)),
                            soy.atoms.Vector((-0.01757866144180298, -0.8127994537353516, -0.5822321176528931)),
                            soy.atoms.Position((0.3474087119102478, 0.08671217411756516)), soy.atoms.Vector((0, 0, 0)))
    v180 = soy.atoms.Vertex(soy.atoms.Position((-8.742277657347586e-08, -1.0, -1.0)),
                            soy.atoms.Vector((-0.01757866144180298, -0.8127994537353516, -0.5822321176528931)),
                            soy.atoms.Position((0.3474087119102478, 0.08671217411756516)), soy.atoms.Vector((0, 0, 0)))
    v181 = soy.atoms.Vertex(soy.atoms.Position((-5.716119844123568e-08, -0.8001823425292969, 1.0836149454116821)),
                            soy.atoms.Vector((-0.12988677620887756, -0.9755851626396179, 0.17694631218910217)),
                            soy.atoms.Position((0.3328333795070648, 0.2146204710006714)), soy.atoms.Vector((0, 0, 0)))
    v182 = soy.atoms.Vertex(soy.atoms.Position((-0.4527122974395752, -0.45271211862564087, 1.052250623703003)),
                            soy.atoms.Vector((-0.774407148361206, -0.6091799736022949, 0.17069002985954285)),
                            soy.atoms.Position((0.32837894558906555, 0.21060481667518616)), soy.atoms.Vector((0, 0, 0)))
    v183 = soy.atoms.Vertex(soy.atoms.Position((-0.4527122974395752, -0.45271211862564087, 1.052250623703003)),
                            soy.atoms.Vector((-0.774407148361206, -0.6091799736022949, 0.17069002985954285)),
                            soy.atoms.Position((0.32837894558906555, 0.21060481667518616)), soy.atoms.Vector((0, 0, 0)))
    v184 = soy.atoms.Vertex(soy.atoms.Position((-0.640231728553772, 7.634686660651369e-09, 1.052250623703003)),
                            soy.atoms.Vector((-0.9812921285629272, 0.0, 0.19241920113563538)),
                            soy.atoms.Position((0.33578529953956604, 0.20685933530330658)), soy.atoms.Vector((0, 0, 0)))
    v185 = soy.atoms.Vertex(soy.atoms.Position((-1.0, 1.1924880638503055e-08, -1.0)),
                            soy.atoms.Vector((-0.825678288936615, 0.0, -0.5641041398048401)),
                            soy.atoms.Position((0.3358173370361328, 0.0829748809337616)), soy.atoms.Vector((0, 0, 0)))
    v186 = soy.atoms.Vertex(soy.atoms.Position((-0.640231728553772, 7.634686660651369e-09, 1.052250623703003)),
                            soy.atoms.Vector((-0.9812921285629272, 0.0, 0.19241920113563538)),
                            soy.atoms.Position((0.33578529953956604, 0.20685933530330658)), soy.atoms.Vector((0, 0, 0)))
    v187 = soy.atoms.Vertex(soy.atoms.Position((-0.4527122974395752, 0.45271211862564087, 1.052250623703003)),
                            soy.atoms.Vector((-0.6938688158988953, 0.6938688158988953, 0.19241920113563538)),
                            soy.atoms.Position((0.3441375494003296, 0.2036278396844864)), soy.atoms.Vector((0, 0, 0)))
    v188 = soy.atoms.Vertex(soy.atoms.Position((-0.70710688829422, 0.7071066498756409, -1.0)),
                            soy.atoms.Vector((-0.5838496088981628, 0.5838496088981628, -0.5641041398048401)),
                            soy.atoms.Position((0.33285850286483765, 0.06971260160207748)), soy.atoms.Vector((0, 0, 0)))
    v189 = soy.atoms.Vertex(soy.atoms.Position((-0.4527122974395752, 0.45271211862564087, 1.052250623703003)),
                            soy.atoms.Vector((-0.6938688158988953, 0.6938688158988953, 0.19241920113563538)),
                            soy.atoms.Position((0.3441375494003296, 0.2036278396844864)), soy.atoms.Vector((0, 0, 0)))
    v190 = soy.atoms.Vertex(soy.atoms.Position((-1.190366027969958e-09, 0.640231728553772, 1.052250623703003)),
                            soy.atoms.Vector((-0.11682485044002533, 0.9783623814582825, 0.17069002985954285)),
                            soy.atoms.Position((0.35524094104766846, 0.20218642055988312)), soy.atoms.Vector((0, 0, 0)))
    v191 = soy.atoms.Vertex(soy.atoms.Position((0.0, 1.0, -1.0)),
                            soy.atoms.Vector((0.0, 0.825678288936615, -0.5641041398048401)),
                            soy.atoms.Position((0.3339645564556122, 0.04476860910654068)), soy.atoms.Vector((0, 0, 0)))
    v192 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.5754127502441406, 3.803668975830078)),
                            soy.atoms.Vector((0.0, 1.0, 0.0)),
                            soy.atoms.Position((0.2142072468996048, 0.056988880038261414)), soy.atoms.Vector((0, 0, 0)))
    v193 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, 1.1126011610031128, 3.803668975830078)),
                            soy.atoms.Vector((0.7070833444595337, 0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.24132287502288818, 0.07176600396633148)), soy.atoms.Vector((0, 0, 0)))
    v194 = soy.atoms.Vertex(soy.atoms.Position((0.7740284204483032, 0.7853434085845947, 2.686342239379883)),
                            soy.atoms.Vector((0.5293435454368591, 0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.2204035222530365, 0.09558363258838654)), soy.atoms.Vector((0, 0, 0)))
    v195 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.1126008033752441, 4.92099666595459)),
                            soy.atoms.Vector((0.0, 0.748588502407074, 0.6629840731620789)),
                            soy.atoms.Position((0.23088151216506958, 0.012395179830491543)),
                            soy.atoms.Vector((0, 0, 0)))
    v196 = soy.atoms.Vertex(soy.atoms.Position((0.7740284204483032, 0.7853434085845947, 4.92099666595459)),
                            soy.atoms.Vector((0.5293435454368591, 0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.2664477527141571, 0.03352435678243637)), soy.atoms.Vector((0, 0, 0)))
    v197 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, 1.1126011610031128, 3.803668975830078)),
                            soy.atoms.Vector((0.7070833444595337, 0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.24132287502288818, 0.07176600396633148)), soy.atoms.Vector((0, 0, 0)))
    v198 = soy.atoms.Vertex(soy.atoms.Position((0.7740284204483032, 0.7853434085845947, 4.92099666595459)),
                            soy.atoms.Vector((0.5293435454368591, 0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.2664477527141571, 0.03352435678243637)), soy.atoms.Vector((0, 0, 0)))
    v199 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -0.0047261822037398815, 4.92099666595459)),
                            soy.atoms.Vector((0.748588502407074, 0.0, 0.6629840731620789)),
                            soy.atoms.Position((0.2965916693210602, 0.05921382084488869)), soy.atoms.Vector((0, 0, 0)))
    v200 = soy.atoms.Vertex(soy.atoms.Position((1.5640983581542969, -0.0047261822037398815, 3.803668975830078)),
                            soy.atoms.Vector((1.0, 0.0, 0.0)),
                            soy.atoms.Position((0.26693272590637207, 0.0941304862499237)), soy.atoms.Vector((0, 0, 0)))
    v201 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, 1.1126011610031128, 3.803668975830078)),
                            soy.atoms.Vector((0.7070833444595337, 0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.24132287502288818, 0.07176600396633148)), soy.atoms.Vector((0, 0, 0)))
    v202 = soy.atoms.Vertex(soy.atoms.Position((1.5640983581542969, -0.0047261822037398815, 3.803668975830078)),
                            soy.atoms.Vector((1.0, 0.0, 0.0)),
                            soy.atoms.Position((0.26693272590637207, 0.0941304862499237)), soy.atoms.Vector((0, 0, 0)))
    v203 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -0.0047261822037398815, 2.686342239379883)),
                            soy.atoms.Vector((0.748588502407074, 0.0, -0.6629840731620789)),
                            soy.atoms.Position((0.2357088029384613, 0.11679655313491821)), soy.atoms.Vector((0, 0, 0)))
    v204 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -0.0047261822037398815, 2.686342239379883)),
                            soy.atoms.Vector((0.748588502407074, 0.0, -0.6629840731620789)),
                            soy.atoms.Position((0.2357088029384613, 0.11679655313491821)), soy.atoms.Vector((0, 0, 0)))
    v205 = soy.atoms.Vertex(soy.atoms.Position((1.5640983581542969, -0.0047261822037398815, 3.803668975830078)),
                            soy.atoms.Vector((1.0, 0.0, 0.0)),
                            soy.atoms.Position((0.26693272590637207, 0.0941304862499237)), soy.atoms.Vector((0, 0, 0)))
    v206 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -1.1220535039901733, 3.803668975830078)),
                            soy.atoms.Vector((0.7070833444595337, -0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.2988373339176178, 0.13461822271347046)), soy.atoms.Vector((0, 0, 0)))
    v207 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -0.0047261822037398815, 4.92099666595459)),
                            soy.atoms.Vector((0.748588502407074, 0.0, 0.6629840731620789)),
                            soy.atoms.Position((0.2965916693210602, 0.05921382084488869)), soy.atoms.Vector((0, 0, 0)))
    v208 = soy.atoms.Vertex(soy.atoms.Position((0.7740283012390137, -0.7947957515716553, 4.92099666595459)),
                            soy.atoms.Vector((0.5293435454368591, -0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.3282824158668518, 0.08495312929153442)), soy.atoms.Vector((0, 0, 0)))
    v209 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -1.1220535039901733, 3.803668975830078)),
                            soy.atoms.Vector((0.7070833444595337, -0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.2988373339176178, 0.13461822271347046)), soy.atoms.Vector((0, 0, 0)))
    v210 = soy.atoms.Vertex(soy.atoms.Position((1.1012861728668213, -1.1220535039901733, 3.803668975830078)),
                            soy.atoms.Vector((0.7070833444595337, -0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.05025875195860863, 0.1478371024131775)), soy.atoms.Vector((0, 0, 0)))
    v211 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -1.5848652124404907, 3.803668975830078)),
                            soy.atoms.Vector((0.0, -1.0, 0.0)),
                            soy.atoms.Position((0.0892295092344284, 0.09508739411830902)), soy.atoms.Vector((0, 0, 0)))
    v212 = soy.atoms.Vertex(soy.atoms.Position((-0.016041234135627747, -1.1220531463623047, 2.686342239379883)),
                            soy.atoms.Vector((0.0, -0.748588502407074, -0.6629840731620789)),
                            soy.atoms.Position((0.12589187920093536, 0.1205931082367897)), soy.atoms.Vector((0, 0, 0)))
    v213 = soy.atoms.Vertex(soy.atoms.Position((0.7740283012390137, -0.7947957515716553, 4.92099666595459)),
                            soy.atoms.Vector((0.5293435454368591, -0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((4.8266916564898565e-05, 0.07856482267379761)),
                            soy.atoms.Vector((0, 0, 0)))
    v214 = soy.atoms.Vertex(soy.atoms.Position((-0.016041234135627747, -1.1220531463623047, 4.92099666595459)),
                            soy.atoms.Vector((0.0, -0.748588502407074, 0.6629840731620789)),
                            soy.atoms.Position((0.046560660004615784, 0.04192383959889412)),
                            soy.atoms.Vector((0, 0, 0)))
    v215 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -1.5848652124404907, 3.803668975830078)),
                            soy.atoms.Vector((0.0, -1.0, 0.0)),
                            soy.atoms.Position((0.0892295092344284, 0.09508739411830902)), soy.atoms.Vector((0, 0, 0)))
    v216 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, -1.5848652124404907, 3.803668975830078)),
                            soy.atoms.Vector((0.0, -1.0, 0.0)),
                            soy.atoms.Position((0.0892295092344284, 0.09508739411830902)), soy.atoms.Vector((0, 0, 0)))
    v217 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -1.1220531463623047, 3.803668975830078)),
                            soy.atoms.Vector((-0.7070833444595337, -0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.12132260203361511, 0.06741581857204437)), soy.atoms.Vector((0, 0, 0)))
    v218 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, -0.7947955131530762, 2.686342239379883)),
                            soy.atoms.Vector((-0.5293435454368591, -0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.14164607226848602, 0.09647044539451599)), soy.atoms.Vector((0, 0, 0)))
    v219 = soy.atoms.Vertex(soy.atoms.Position((-0.016041234135627747, -1.1220531463623047, 4.92099666595459)),
                            soy.atoms.Vector((0.0, -0.748588502407074, 0.6629840731620789)),
                            soy.atoms.Position((0.046560660004615784, 0.04192383959889412)),
                            soy.atoms.Vector((0, 0, 0)))
    v220 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, -0.7947955131530762, 4.92099666595459)),
                            soy.atoms.Vector((-0.5293435454368591, -0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.09350647032260895, 0.01426364853978157)), soy.atoms.Vector((0, 0, 0)))
    v221 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -1.1220531463623047, 3.803668975830078)),
                            soy.atoms.Vector((-0.7070833444595337, -0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.12132260203361511, 0.06741581857204437)), soy.atoms.Vector((0, 0, 0)))
    v222 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -1.1220531463623047, 3.803668975830078)),
                            soy.atoms.Vector((-0.7070833444595337, -0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.12132260203361511, 0.06741581857204437)), soy.atoms.Vector((0, 0, 0)))
    v223 = soy.atoms.Vertex(soy.atoms.Position((-1.5961802005767822, -0.0047261822037398815, 3.803668975830078)),
                            soy.atoms.Vector((-1.0, 0.0, 0.0)),
                            soy.atoms.Position((0.15326373279094696, 0.05347014591097832)), soy.atoms.Vector((0, 0, 0)))
    v224 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -0.0047261822037398815, 2.686342239379883)),
                            soy.atoms.Vector((-0.748588502407074, 0.0, -0.6629840731620789)),
                            soy.atoms.Position((0.16129980981349945, 0.0836634412407875)), soy.atoms.Vector((0, 0, 0)))
    v225 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, -0.7947955131530762, 4.92099666595459)),
                            soy.atoms.Vector((-0.5293435454368591, -0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.09350647032260895, 0.01426364853978157)), soy.atoms.Vector((0, 0, 0)))
    v226 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -0.0047261822037398815, 4.92099666595459)),
                            soy.atoms.Vector((-0.748588502407074, 0.0, 0.6629840731620789)),
                            soy.atoms.Position((0.1418866664171219, 4.829086901736446e-05)),
                            soy.atoms.Vector((0, 0, 0)))
    v227 = soy.atoms.Vertex(soy.atoms.Position((-1.5961802005767822, -0.0047261822037398815, 3.803668975830078)),
                            soy.atoms.Vector((-1.0, 0.0, 0.0)),
                            soy.atoms.Position((0.15326373279094696, 0.05347014591097832)), soy.atoms.Vector((0, 0, 0)))
    v228 = soy.atoms.Vertex(soy.atoms.Position((-1.5961802005767822, -0.0047261822037398815, 3.803668975830078)),
                            soy.atoms.Vector((-1.0, 0.0, 0.0)),
                            soy.atoms.Position((0.15326373279094696, 0.05347014591097832)), soy.atoms.Vector((0, 0, 0)))
    v229 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, 1.1126008033752441, 3.803668975830078)),
                            soy.atoms.Vector((-0.7070833444595337, 0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.18463541567325592, 0.0504889078438282)), soy.atoms.Vector((0, 0, 0)))
    v230 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, 0.7853432893753052, 2.686342239379883)),
                            soy.atoms.Vector((-0.5293435454368591, 0.5293435454368591, -0.6629840731620789)),
                            soy.atoms.Position((0.18209107220172882, 0.07979989051818848)), soy.atoms.Vector((0, 0, 0)))
    v231 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, -0.0047261822037398815, 4.92099666595459)),
                            soy.atoms.Vector((-0.748588502407074, 0.0, 0.6629840731620789)),
                            soy.atoms.Position((0.1418866664171219, 4.829086901736446e-05)),
                            soy.atoms.Vector((0, 0, 0)))
    v232 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, 0.7853432893753052, 4.92099666595459)),
                            soy.atoms.Vector((-0.5293435454368591, 0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.18878695368766785, 4.8266916564898565e-05)),
                            soy.atoms.Vector((0, 0, 0)))
    v233 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, 1.1126008033752441, 3.803668975830078)),
                            soy.atoms.Vector((-0.7070833444595337, 0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.18463541567325592, 0.0504889078438282)), soy.atoms.Vector((0, 0, 0)))
    v234 = soy.atoms.Vertex(soy.atoms.Position((-0.8061108589172363, 0.7853432893753052, 4.92099666595459)),
                            soy.atoms.Vector((-0.5293435454368591, 0.5293435454368591, 0.6629840731620789)),
                            soy.atoms.Position((0.18878695368766785, 4.8266916564898565e-05)),
                            soy.atoms.Vector((0, 0, 0)))
    v235 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.1126008033752441, 4.92099666595459)),
                            soy.atoms.Vector((0.0, 0.748588502407074, 0.6629840731620789)),
                            soy.atoms.Position((0.23088151216506958, 0.012395179830491543)),
                            soy.atoms.Vector((0, 0, 0)))
    v236 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.5754127502441406, 3.803668975830078)),
                            soy.atoms.Vector((0.0, 1.0, 0.0)),
                            soy.atoms.Position((0.2142072468996048, 0.056988880038261414)), soy.atoms.Vector((0, 0, 0)))
    v237 = soy.atoms.Vertex(soy.atoms.Position((-1.1333682537078857, 1.1126008033752441, 3.803668975830078)),
                            soy.atoms.Vector((-0.7070833444595337, 0.7070833444595337, 0.0)),
                            soy.atoms.Position((0.18463541567325592, 0.0504889078438282)), soy.atoms.Vector((0, 0, 0)))
    v238 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.5754127502441406, 3.803668975830078)),
                            soy.atoms.Vector((0.0, 1.0, 0.0)),
                            soy.atoms.Position((0.2142072468996048, 0.056988880038261414)), soy.atoms.Vector((0, 0, 0)))
    v239 = soy.atoms.Vertex(soy.atoms.Position((-0.016041278839111328, 1.1126008033752441, 2.686342239379883)),
                            soy.atoms.Vector((0.0, 0.748588502407074, -0.6629840731620789)),
                            soy.atoms.Position((0.20222534239292145, 0.08384476602077484)), soy.atoms.Vector((0, 0, 0)))
    f0 = soy.atoms.Face(v0, v1, v2, matIndex0)
    room[key].append(f0)
    f1 = soy.atoms.Face(v3, v4, v5, matIndex1)
    room[key].append(f1)
    f2 = soy.atoms.Face(v6, v7, v8, matIndex0)
    room[key].append(f2)
    f3 = soy.atoms.Face(v9, v10, v11, matIndex0)
    room[key].append(f3)
    f4 = soy.atoms.Face(v12, v13, v14, matIndex1)
    room[key].append(f4)
    f5 = soy.atoms.Face(v15, v16, v17, matIndex0)
    room[key].append(f5)
    f6 = soy.atoms.Face(v18, v19, v20, matIndex0)
    room[key].append(f6)
    f7 = soy.atoms.Face(v21, v22, v23, matIndex1)
    room[key].append(f7)
    f8 = soy.atoms.Face(v24, v25, v26, matIndex0)
    room[key].append(f8)
    f9 = soy.atoms.Face(v27, v28, v29, matIndex0)
    room[key].append(f9)
    f10 = soy.atoms.Face(v30, v31, v32, matIndex1)
    room[key].append(f10)
    f11 = soy.atoms.Face(v33, v34, v35, matIndex0)
    room[key].append(f11)
    f12 = soy.atoms.Face(v36, v37, v38, matIndex0)
    room[key].append(f12)
    f13 = soy.atoms.Face(v39, v40, v41, matIndex1)
    room[key].append(f13)
    f14 = soy.atoms.Face(v42, v43, v44, matIndex0)
    room[key].append(f14)
    f15 = soy.atoms.Face(v45, v46, v47, matIndex0)
    room[key].append(f15)
    f16 = soy.atoms.Face(v48, v49, v50, matIndex1)
    room[key].append(f16)
    f17 = soy.atoms.Face(v51, v52, v53, matIndex0)
    room[key].append(f17)
    f18 = soy.atoms.Face(v54, v55, v56, matIndex0)
    room[key].append(f18)
    f19 = soy.atoms.Face(v57, v58, v59, matIndex1)
    room[key].append(f19)
    f20 = soy.atoms.Face(v60, v61, v62, matIndex0)
    room[key].append(f20)
    f21 = soy.atoms.Face(v63, v64, v65, matIndex0)
    room[key].append(f21)
    f22 = soy.atoms.Face(v66, v67, v68, matIndex1)
    room[key].append(f22)
    f23 = soy.atoms.Face(v69, v70, v71, matIndex0)
    room[key].append(f23)
    f24 = soy.atoms.Face(v72, v73, v74, matIndex1)
    room[key].append(f24)
    f25 = soy.atoms.Face(v75, v76, v77, matIndex1)
    room[key].append(f25)
    f26 = soy.atoms.Face(v78, v79, v80, matIndex1)
    room[key].append(f26)
    f27 = soy.atoms.Face(v81, v82, v83, matIndex1)
    room[key].append(f27)
    f28 = soy.atoms.Face(v84, v85, v86, matIndex1)
    room[key].append(f28)
    f29 = soy.atoms.Face(v87, v88, v89, matIndex1)
    room[key].append(f29)
    f30 = soy.atoms.Face(v90, v91, v92, matIndex1)
    room[key].append(f30)
    f31 = soy.atoms.Face(v93, v94, v95, matIndex1)
    room[key].append(f31)
    f32 = soy.atoms.Face(v96, v97, v98, matIndex1)
    room[key].append(f32)
    f33 = soy.atoms.Face(v99, v100, v101, matIndex1)
    room[key].append(f33)
    f34 = soy.atoms.Face(v102, v103, v104, matIndex1)
    room[key].append(f34)
    f35 = soy.atoms.Face(v105, v106, v107, matIndex1)
    room[key].append(f35)
    f36 = soy.atoms.Face(v108, v109, v110, matIndex1)
    room[key].append(f36)
    f37 = soy.atoms.Face(v111, v112, v113, matIndex1)
    room[key].append(f37)
    f38 = soy.atoms.Face(v114, v115, v116, matIndex1)
    room[key].append(f38)
    f39 = soy.atoms.Face(v117, v118, v119, matIndex1)
    room[key].append(f39)
    f40 = soy.atoms.Face(v120, v121, v122, matIndex1)
    room[key].append(f40)
    f41 = soy.atoms.Face(v123, v124, v125, matIndex1)
    room[key].append(f41)
    f42 = soy.atoms.Face(v126, v127, v128, matIndex1)
    room[key].append(f42)
    f43 = soy.atoms.Face(v129, v130, v131, matIndex1)
    room[key].append(f43)
    f44 = soy.atoms.Face(v132, v133, v134, matIndex1)
    room[key].append(f44)
    f45 = soy.atoms.Face(v135, v136, v137, matIndex1)
    room[key].append(f45)
    f46 = soy.atoms.Face(v138, v139, v140, matIndex1)
    room[key].append(f46)
    f47 = soy.atoms.Face(v141, v142, v143, matIndex1)
    room[key].append(f47)
    f48 = soy.atoms.Face(v144, v145, v146, matIndex1)
    room[key].append(f48)
    f49 = soy.atoms.Face(v147, v148, v149, matIndex1)
    room[key].append(f49)
    f50 = soy.atoms.Face(v150, v151, v152, matIndex1)
    room[key].append(f50)
    f51 = soy.atoms.Face(v153, v154, v155, matIndex1)
    room[key].append(f51)
    f52 = soy.atoms.Face(v156, v157, v158, matIndex1)
    room[key].append(f52)
    f53 = soy.atoms.Face(v159, v160, v161, matIndex1)
    room[key].append(f53)
    f54 = soy.atoms.Face(v162, v163, v164, matIndex1)
    room[key].append(f54)
    f55 = soy.atoms.Face(v165, v166, v167, matIndex1)
    room[key].append(f55)
    f56 = soy.atoms.Face(v168, v169, v170, matIndex0)
    room[key].append(f56)
    f57 = soy.atoms.Face(v171, v172, v173, matIndex0)
    room[key].append(f57)
    f58 = soy.atoms.Face(v174, v175, v176, matIndex0)
    room[key].append(f58)
    f59 = soy.atoms.Face(v177, v178, v179, matIndex0)
    room[key].append(f59)
    f60 = soy.atoms.Face(v180, v181, v182, matIndex0)
    room[key].append(f60)
    f61 = soy.atoms.Face(v183, v184, v185, matIndex0)
    room[key].append(f61)
    f62 = soy.atoms.Face(v186, v187, v188, matIndex0)
    room[key].append(f62)
    f63 = soy.atoms.Face(v189, v190, v191, matIndex0)
    room[key].append(f63)
    f64 = soy.atoms.Face(v192, v193, v194, matIndex1)
    room[key].append(f64)
    f65 = soy.atoms.Face(v195, v196, v197, matIndex1)
    room[key].append(f65)
    f66 = soy.atoms.Face(v198, v199, v200, matIndex1)
    room[key].append(f66)
    f67 = soy.atoms.Face(v201, v202, v203, matIndex1)
    room[key].append(f67)
    f68 = soy.atoms.Face(v204, v205, v206, matIndex1)
    room[key].append(f68)
    f69 = soy.atoms.Face(v207, v208, v209, matIndex1)
    room[key].append(f69)
    f70 = soy.atoms.Face(v210, v211, v212, matIndex1)
    room[key].append(f70)
    f71 = soy.atoms.Face(v213, v214, v215, matIndex1)
    room[key].append(f71)
    f72 = soy.atoms.Face(v216, v217, v218, matIndex1)
    room[key].append(f72)
    f73 = soy.atoms.Face(v219, v220, v221, matIndex1)
    room[key].append(f73)
    f74 = soy.atoms.Face(v222, v223, v224, matIndex1)
    room[key].append(f74)
    f75 = soy.atoms.Face(v225, v226, v227, matIndex1)
    room[key].append(f75)
    f76 = soy.atoms.Face(v228, v229, v230, matIndex1)
    room[key].append(f76)
    f77 = soy.atoms.Face(v231, v232, v233, matIndex1)
    room[key].append(f77)
    f78 = soy.atoms.Face(v234, v235, v236, matIndex1)
    room[key].append(f78)
    f79 = soy.atoms.Face(v237, v238, v239, matIndex1)
    room[key].append(f79)

    return

def makeHills(room) :
	m1 = soy.materials.Colored("brown")
	m2 = soy.materials.Colored("brown")
	m3 = soy.materials.Colored("brown")
	m4 = soy.materials.Colored("brown")

	a = 1 / sqrt(24)
	b = sqrt(2 / 24)
	z = 1 / sqrt(16)
	size = 11

	v1 = soy.atoms.Vertex(soy.atoms.Position((1 * size, 0, -z)), soy.atoms.Vector((a, 0, b)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((1, 0, 0)))
	v2 = soy.atoms.Vertex(soy.atoms.Position((0, 1 * size , z)), soy.atoms.Vector((a, 0, b)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((1, 0, 0)))
	v3 = soy.atoms.Vertex(soy.atoms.Position((0, -1 * size, z)), soy.atoms.Vector((a, 0, b)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((1, 0, 0)))

	v4 = soy.atoms.Vertex(soy.atoms.Position((-1 * size, 0, -z)), soy.atoms.Vector((-a, 0, b)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((1, 0, 0)))
	v5 = soy.atoms.Vertex(soy.atoms.Position((0, -1 * size, z)), soy.atoms.Vector((-a, 0, b)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((1, 0, 0)))
	v6 = soy.atoms.Vertex(soy.atoms.Position((0, 1 * size, z)), soy.atoms.Vector((-a, 0, b)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((1, 0, 0)))

	v7 = soy.atoms.Vertex(soy.atoms.Position((1, 0, -z)), soy.atoms.Vector((0, a, -b)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((1, 0, 0)))
	v8 = soy.atoms.Vertex(soy.atoms.Position((-1, 0, -z)), soy.atoms.Vector((0, a, -b)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((1, 0, 0)))
	v9 = soy.atoms.Vertex(soy.atoms.Position((0, 1, z)), soy.atoms.Vector((0, a, -b)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((1, 0, 0)))

	v10 = soy.atoms.Vertex(soy.atoms.Position((1, 0, -z)), soy.atoms.Vector((0, -a, -b)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((1, 0, 0)))
	v11 = soy.atoms.Vertex(soy.atoms.Position((0, -1, z)), soy.atoms.Vector((0, -a, -b)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((1, 0, 0)))
	v12 = soy.atoms.Vertex(soy.atoms.Position((-1, 0, -z)), soy.atoms.Vector((0, -a, -b))
						   , soy.atoms.Position((0, 0)), soy.atoms.Vector((1, 0, 0)))

	f1 = soy.atoms.Face(v1, v2, v3, m1)
	f2 = soy.atoms.Face(v4, v5, v6, m2)


	room['mesh'] = soy.bodies.Mesh()
	room['mesh'].append(f1)
	room['mesh'].append(f2)
	room['mesh'].position = soy.atoms.Position((-6, -1, -10))
	room['mesh'].toggleField()

	return

def importedFromBlender(room) :
	room['importedMesh'] = soy.bodies.Mesh()
	m1 = soy.materials.Colored("red")

	v0 = soy.atoms.Vertex(soy.atoms.Position((1.0, -1.0, -1.0)),
						  soy.atoms.Vector((0.5773491859436035, -0.5773491859436035, -0.5773491859436035)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v1 = soy.atoms.Vertex(soy.atoms.Position((-0.9999996423721313, 1.0000003576278687, -1.0)),
						  soy.atoms.Vector((-0.5773491859436035, 0.5773491859436035, -0.5773491859436035)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v2 = soy.atoms.Vertex(soy.atoms.Position((1.0, 0.9999999403953552, -1.0)),
						  soy.atoms.Vector((0.5773491859436035, 0.5773491859436035, -0.5773491859436035)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v3 = soy.atoms.Vertex(soy.atoms.Position((-0.9999999403953552, 1.0, 1.0)),
						  soy.atoms.Vector((-0.5773491859436035, 0.5773491859436035, 0.5773491859436035)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v4 = soy.atoms.Vertex(soy.atoms.Position((0.9999993443489075, -1.0000005960464478, 1.0)),
						  soy.atoms.Vector((0.5773491859436035, -0.5773491859436035, 0.5773491859436035)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v5 = soy.atoms.Vertex(soy.atoms.Position((1.0000004768371582, 0.999999463558197, 1.0)),
						  soy.atoms.Vector((0.5773491859436035, 0.5773491859436035, 0.5773491859436035)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v6 = soy.atoms.Vertex(soy.atoms.Position((1.0000004768371582, 0.999999463558197, 1.0)),
						  soy.atoms.Vector((0.5773491859436035, 0.5773491859436035, 0.5773491859436035)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v7 = soy.atoms.Vertex(soy.atoms.Position((1.0, -1.0, -1.0)),
						  soy.atoms.Vector((0.5773491859436035, -0.5773491859436035, -0.5773491859436035)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v8 = soy.atoms.Vertex(soy.atoms.Position((1.0, 0.9999999403953552, -1.0)),
						  soy.atoms.Vector((0.5773491859436035, 0.5773491859436035, -0.5773491859436035)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v9 = soy.atoms.Vertex(soy.atoms.Position((0.9999993443489075, -1.0000005960464478, 1.0)),
						  soy.atoms.Vector((0.5773491859436035, -0.5773491859436035, 0.5773491859436035)),
						  soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v10 = soy.atoms.Vertex(soy.atoms.Position((-1.0000001192092896, -0.9999998211860657, -1.0)),
						   soy.atoms.Vector((-0.5773491859436035, -0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v11 = soy.atoms.Vertex(soy.atoms.Position((1.0, -1.0, -1.0)),
						   soy.atoms.Vector((0.5773491859436035, -0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v12 = soy.atoms.Vertex(soy.atoms.Position((-1.0000001192092896, -0.9999998211860657, -1.0)),
						   soy.atoms.Vector((-0.5773491859436035, -0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v13 = soy.atoms.Vertex(soy.atoms.Position((-0.9999999403953552, 1.0, 1.0)),
						   soy.atoms.Vector((-0.5773491859436035, 0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v14 = soy.atoms.Vertex(soy.atoms.Position((-0.9999996423721313, 1.0000003576278687, -1.0)),
						   soy.atoms.Vector((-0.5773491859436035, 0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v15 = soy.atoms.Vertex(soy.atoms.Position((1.0, 0.9999999403953552, -1.0)),
						   soy.atoms.Vector((0.5773491859436035, 0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v16 = soy.atoms.Vertex(soy.atoms.Position((-0.9999999403953552, 1.0, 1.0)),
						   soy.atoms.Vector((-0.5773491859436035, 0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v17 = soy.atoms.Vertex(soy.atoms.Position((1.0000004768371582, 0.999999463558197, 1.0)),
						   soy.atoms.Vector((0.5773491859436035, 0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v18 = soy.atoms.Vertex(soy.atoms.Position((1.0, -1.0, -1.0)),
						   soy.atoms.Vector((0.5773491859436035, -0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v19 = soy.atoms.Vertex(soy.atoms.Position((-1.0000001192092896, -0.9999998211860657, -1.0)),
						   soy.atoms.Vector((-0.5773491859436035, -0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v20 = soy.atoms.Vertex(soy.atoms.Position((-0.9999996423721313, 1.0000003576278687, -1.0)),
						   soy.atoms.Vector((-0.5773491859436035, 0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v21 = soy.atoms.Vertex(soy.atoms.Position((-0.9999999403953552, 1.0, 1.0)),
						   soy.atoms.Vector((-0.5773491859436035, 0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v22 = soy.atoms.Vertex(soy.atoms.Position((-1.0000003576278687, -0.9999996423721313, 1.0)),
						   soy.atoms.Vector((-0.5773491859436035, -0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v23 = soy.atoms.Vertex(soy.atoms.Position((0.9999993443489075, -1.0000005960464478, 1.0)),
						   soy.atoms.Vector((0.5773491859436035, -0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v24 = soy.atoms.Vertex(soy.atoms.Position((1.0000004768371582, 0.999999463558197, 1.0)),
						   soy.atoms.Vector((0.5773491859436035, 0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v25 = soy.atoms.Vertex(soy.atoms.Position((0.9999993443489075, -1.0000005960464478, 1.0)),
						   soy.atoms.Vector((0.5773491859436035, -0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v26 = soy.atoms.Vertex(soy.atoms.Position((1.0, -1.0, -1.0)),
						   soy.atoms.Vector((0.5773491859436035, -0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v27 = soy.atoms.Vertex(soy.atoms.Position((0.9999993443489075, -1.0000005960464478, 1.0)),
						   soy.atoms.Vector((0.5773491859436035, -0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v28 = soy.atoms.Vertex(soy.atoms.Position((-1.0000003576278687, -0.9999996423721313, 1.0)),
						   soy.atoms.Vector((-0.5773491859436035, -0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v29 = soy.atoms.Vertex(soy.atoms.Position((-1.0000001192092896, -0.9999998211860657, -1.0)),
						   soy.atoms.Vector((-0.5773491859436035, -0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v30 = soy.atoms.Vertex(soy.atoms.Position((-1.0000001192092896, -0.9999998211860657, -1.0)),
						   soy.atoms.Vector((-0.5773491859436035, -0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v31 = soy.atoms.Vertex(soy.atoms.Position((-1.0000003576278687, -0.9999996423721313, 1.0)),
						   soy.atoms.Vector((-0.5773491859436035, -0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v32 = soy.atoms.Vertex(soy.atoms.Position((-0.9999999403953552, 1.0, 1.0)),
						   soy.atoms.Vector((-0.5773491859436035, 0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v33 = soy.atoms.Vertex(soy.atoms.Position((1.0, 0.9999999403953552, -1.0)),
						   soy.atoms.Vector((0.5773491859436035, 0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v34 = soy.atoms.Vertex(soy.atoms.Position((-0.9999996423721313, 1.0000003576278687, -1.0)),
						   soy.atoms.Vector((-0.5773491859436035, 0.5773491859436035, -0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	v35 = soy.atoms.Vertex(soy.atoms.Position((-0.9999999403953552, 1.0, 1.0)),
						   soy.atoms.Vector((-0.5773491859436035, 0.5773491859436035, 0.5773491859436035)),
						   soy.atoms.Position((0, 0)), soy.atoms.Vector((0, 0, 0)))
	f0 = soy.atoms.Face(v0, v1, v2, m1)
	room['importedMesh'].append(f0)
	f1 = soy.atoms.Face(v3, v4, v5, m1)
	room['importedMesh'].append(f1)
	f2 = soy.atoms.Face(v6, v7, v8, m1)
	room['importedMesh'].append(f2)
	f3 = soy.atoms.Face(v9, v10, v11, m1)
	room['importedMesh'].append(f3)
	f4 = soy.atoms.Face(v12, v13, v14, m1)
	room['importedMesh'].append(f4)
	f5 = soy.atoms.Face(v15, v16, v17, m1)
	room['importedMesh'].append(f5)
	f6 = soy.atoms.Face(v18, v19, v20, m1)
	room['importedMesh'].append(f6)
	f7 = soy.atoms.Face(v21, v22, v23, m1)
	room['importedMesh'].append(f7)
	f8 = soy.atoms.Face(v24, v25, v26, m1)
	room['importedMesh'].append(f8)
	f9 = soy.atoms.Face(v27, v28, v29, m1)
	room['importedMesh'].append(f9)
	f10 = soy.atoms.Face(v30, v31, v32, m1)
	room['importedMesh'].append(f10)
	f11 = soy.atoms.Face(v33, v34, v35, m1)
	room['importedMesh'].append(f11)

	room['importedMesh'].position = soy.atoms.Position((-6, 0, 0))
	return