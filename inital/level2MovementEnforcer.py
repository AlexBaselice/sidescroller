import soy
def freezeMesh(item, position, rotation):
    item.velocity = soy.atoms.Vector((0,0,0))
    item.angular_velocity = soy.atoms.Vector((0,0,0))
    item.position = position
    item.rotation = rotation
    return
def freezeSceneMeshes(room):
    freezeMesh(room["RP_Cube2"], soy.atoms.Position((-55.182193756103516,-8.985444068908691,-47.08796691894531)),soy.atoms.Rotation((0.0,-0.0,3.1415962512969973)))
    freezeMesh(room["RP_Cube"], soy.atoms.Position((-66.71241760253906,-8.715718269348145,-34.85189437866211)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    return
