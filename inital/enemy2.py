import soy

def createEnemy(room) :

    reference = soy.bodies.Box()
    reference.position = soy.atoms.Position((70, -3, -3))
    reference.size = soy.atoms.Size((2, 4, 2))
    reference.material = soy.materials.Colored()
    reference.material.ambient = soy.atoms.Color('green')
    reference.material.diffuse = soy.atoms.Color('yellow')
    reference.material.specular = soy.atoms.Color('yellow')

    room['enemy2'] = reference

    enemyX = 9.1
    enemyY = -5.98
    enemyXStart = 68
    enemyXFinish = 78
    enemyZ = 7
    path2 = (soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ))
            )

    cont = soy.controllers.Pathfollower(room, room['enemy2'] , path2 , 5)
    return