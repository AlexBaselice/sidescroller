import soy

def loadLoadingTexture(textures, fastLoad) :
    if fastLoad == 1 :
        textures['loading'] = soy.textures.Texture("media/1by1.png")
        return
    textures['loading'] = soy.textures.Texture("media/loading.png")
    textures['play'] = soy.textures.Texture("media/play.png")
    textures['quit'] = soy.textures.Texture("media/quit.png")
    textures['stone'] = soy.textures.Texture("media/stone.jpg")
    return

def loadTextures(textures, fastLoad) :
    if fastLoad == 1 :
        textures['sky'] = soy.textures.Texture("media/1by1.png")
        textures['grass'] = soy.textures.Texture("media/1by1.png")
        textures['mainMenu'] = soy.textures.Texture("media/1by1.png")
        return
    textures['sky'] = soy.textures.Texture("media/sky.png")
    textures['grass'] = soy.textures.Texture("media/1by1.png")
    textures['mainMenu'] = soy.textures.Texture("media/mainMenu.png")
    textures['wood'] = soy.textures.Texture("media/wood.png")

    textures['cave'] = soy.textures.Texture("media/cave.png")

    return