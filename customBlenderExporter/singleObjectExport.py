# Custom Blener Exporter
# Exports single selected object in blender with normals and UV cords
# Exports curently selected object
import bpy
import bmesh
from mathutils import Vector

fi = open('/home/alex/Documents/SideScroller/customBlenderExporter/modelScriptForPySoy.py', 'w')
        
fi.write("room[key] = soy.bodies.Mesh()")

obj = bpy.context.active_object
me = bpy.context.object.data
uv_layer = me.uv_layers.active.data
i = 0



#Creatng UV dataStructures to store in vertex index
dictUV = dict()
uvList = list()

currentV = 0
for poly in me.polygons:

    # range is used here to show how the polygons reference loops,
    # for convenience 'poly.loop_indices' can be used instead.
    for loop_index in range(poly.loop_start, poly.loop_start + poly.loop_total):
        uvList.append(uv_layer[loop_index].uv)
        #fi.write("    Vertex: %d" % me.loops[loop_index].vertex_index)
        #fi.write("    UV: %r" % uv_layer[loop_index].uv)       
        # debug fi.write("Vertex" + str(currentV) + str(uv_layer[loop_index].uv))
        # debugfi.write("\n")
        currentV += 1 
        
#currentV = 0        
#for cord in uvList:
#    fi.write("Vertex" + str(currentV) + " uv " + str(uvList[currentV]))
#    fi.write("\n")
#    currentV += 1 

currentV = 0
for f in obj.data.polygons:
    j = 0
    for idx in f.vertices:
        fi.write("v" + str(currentV) + " = soy.atoms.Vertex(soy.atoms.Position((" + str(obj.data.vertices[idx].co[0]))
        fi.write("," + str(obj.data.vertices[idx].co[1]))  
        fi.write("," + str(obj.data.vertices[idx].co[2]) + ")),")
        fi.write("soy.atoms.Vector((")
        fi.write(str(obj.data.vertices[idx].normal.x) + ",")
        fi.write(str(obj.data.vertices[idx].normal.y) + ",")
        fi.write(str(obj.data.vertices[idx].normal.z) + ")),")                
        fi.write("soy.atoms.Position((")
        
        fi.write(str(uvList[currentV][0]) + "," + str(uvList[currentV][1]))
        fi.write(")),")                  
        fi.write("soy.atoms.Vector((0, 0, 0))) \n")
        j += 1
        currentV += 1
    i += 1
    
i = 0  
currentV = 0  
for f in obj.data.polygons:
    j = 0
    fi.write("f" + str(i) + " = soy.atoms.Face(")
    for idx in f.vertices:
        fi.write("v" + str(currentV) + ",")
        currentV += 1
        if j == 2 :
            break
        j += 1
    # Writing he current vertex group as material stand in - join is used to get red of possible white space, remove if unessary
    fi.write(( "matIndex"+ "".join(str(f.material_index).split())) + ")\n")
    fi.write("room[key].append(f" + str(i) + ")" + "\n")
    i += 1

for f in obj.data.polygons: 
    i += 1   
me = bpy.context.object.data
#uv_layer = me.uv.layers.active.data

    

    # range is used here to show how the polygons reference loops,
    # for convenience 'poly.loop_indices' can be used instead.
    #for loop_index in range(poly.loop_start, poly.loop_start + poly.loop_total):
     #   print("    Vertex: %d" % me.loops[loop_index].vertex_index)
      #  print("    UV: %r" % uv_layer[loop_index].uv)    

    
#cubedata = bpy.data.meshes["itemToExport"]
#i = 0
#while i < len(cubedata.vertices):
#    f.write("v" + str(i) + " = soy.atoms.Vertex(soy.atoms.Position(" + str(cubedata.vertices[i].co[0]))
#    f.write("," + str(cubedata.vertices[i].co[1]))  
#    f.write("," + str(cubedata.vertices[i].co[2]) + "))" + "\n")        
#    i += 1 

#fme.uv_layers.active.data




fi.close()    


