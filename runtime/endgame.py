import soy
import sys

from inital import level2ModelLoader

from inital import menu


# End the game if the player goes to high or too low
def checkIfplayerOutOfBounds(room, textures, menuOpen, client):
    # Exiting early if menu is open
    if menuOpen == 1:
        return 1
    playerY = room['player'].position.y
    if playerY > 20 or playerY < -20 and menuOpen != 1:
        returnToMenu(room, textures, client)
        return 1
    else:
        return 0


def returnToMenu(room, textures, client):
    # Set Up Game Over Screen
    menu.changeLoadingScreenBackgroundTexture(room, textures)
    # Setting the past camera x position as score
    oldScore = int(room['cam'].position.x) + 62
    client.window.title = "Ending Score: " + str(oldScore)
    room['cam'].position = soy.atoms.Position((0, -3, 32))
    # Wait some to time to read end screen
    # time.sleep(0.1)
    # Back to main clean up
    del room['player']
    # Go back to main menu
    loadingMat = soy.materials.Textured()
    loadingMat.glowmap = textures['sky']
    loadingMat.shininess = 100
    room['loadingBillboard'].position = soy.atoms.Position((0, -3, 12))
    room['loadingBillboard'].size = soy.atoms.Size((30, 20))
    room['selectionBlock'].position = soy.atoms.Position((0, -3, 22))
    room['selectionBlock'].radius = 0.5
    room['quitBlock'].size = soy.atoms.Size((3, 2, 1))
    room['playBlock'].size = soy.atoms.Size((3, 2, 1))
    gravity = soy.fields.Accelerate(soy.atoms.Vector((0, -9.8, 0)))
    room.addField('gravity', gravity)
    room.removeField('reverseGravity')
    room['gravitySelectionBlock'].position = soy.atoms.Position((-4, -3, 32))
    sys.stdout.write("Game over" + "\n")
    sys.stdout.flush()
    return


def loadLevel2(room, textures, gameState):
    gameState['currentLevel'] = 2
    print(room.keys())
    #level1ToRemove = ['NoMove_Mountain3', 'NoMove_Mountain5', 'NoMove_Rampart_2', 'RP_Cube',
                     # 'DP_Tree_1',
                     #  'DP_Tree_2', 'Plane', 'DP_Tree_3', 'Rampart2', 'NoMove_Rampart',
                     #  'NoMove_Tree', 'NoMove_GroundPlane', 'enemy', 'RP_Cube2', 'RP_Cube3', 'RP_Cube4', 'RP_Cube5', 'RP_Cube6', 'RP_Cube7',
                     #  'RP_Cube8', 'RP_Cube10', 'RP_Cube9']
    level1ToRemove = ['RP_Cube9']
    for obj in level1ToRemove:
        del room[obj]
    #level2ModelLoader.level2LoadModels(room, textures)
    print(room.keys())
    room['player'].position = soy.atoms.Position((45 , -4, -1))
    return
