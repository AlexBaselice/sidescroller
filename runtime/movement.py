# Gameplay Loop - Movement related functions

import soy
import sys

from . import endgame

def preventEnvironmentMoving(room, gameState):

    #room['billboard'].velocity = soy.atoms.Vector((0,0,0))
    #room['billboard'].angular_velocity = soy.atoms.Vector((0,0,0))

    room['loadingBillboard'].velocity = soy.atoms.Vector((0,0,0))
    room['loadingBillboard'].angular_velocity = soy.atoms.Vector((0,0,0))
    room['loadingBillboard'].position = soy.atoms.Position((0, -3, 12))

    room['playBlock'].position = soy.atoms.Position((3, -1, 22))
    preventFromRotatingAndMoving(room['playBlock'])

    room['quitBlock'].position = soy.atoms.Position((3, -5, 22))
    preventFromRotatingAndMoving(room['quitBlock'])

    # freeze w a s d input selection block in place
    room['selectionBlockWorS'].position = soy.atoms.Position((-5.5, room['selectionBlockWorS'].position.y, 35))
    room['selectionBlockAorD'].position = soy.atoms.Position((-7, room['selectionBlockAorD'].position.y, 35))

    # Testing terrain
    if gameState['currentLevel'] == 1:
        preventFromMoving(room['Plane'])

    return

# def preventMenuMoving()

def preventFromRotating(item):
    item.angular_velocity = soy.atoms.Vector((0,0,0))
    return

def preventFromRotatingAndMoving(item) :
    item.velocity = soy.atoms.Vector((0,0,0))
    item.angular_velocity = soy.atoms.Vector((0,0,0))
    return

def preventFromMoving(item) :
    item.velocity = soy.atoms.Vector((0,0,0))
    return

def preventFromMovingZAxis(item):
    item.velocity = soy.atoms.Vector((item.velocity.x,item.velocity.y,0))
    return

def setAllVelocityToZero(room):
    print(room.keys())
    for item in room.keys():
        preventFromRotatingAndMoving(room[item])
    return


# Prevent the player from moving outside of the play volume
def preventPlayerFromLeavingPlayVolume(player, nearBoundZ, farBoundZ, nearBoundX, farBoundX, room, textures, gameState, client):
    if player.position.z < farBoundZ:
        player.velocity = soy.atoms.Vector((player.velocity.x, player.velocity.y, -player.velocity.z))
        player.position = soy.atoms.Position((player.position.x, player.position.y, player.position.z + 2))
    if player.position.z > nearBoundZ:
        player.velocity = soy.atoms.Vector((player.velocity.x, player.velocity.y, -player.velocity.z  ))
        player.position = soy.atoms.Position((player.position.x, player.position.y, player.position.z -7))
    if player.position.x > farBoundX:
        sys.stdout.write("Congratulations, you've won the game!" + "\n")
        endgame.returnToMenu(room, textures, client)
        return 1
    if player.position.x < nearBoundX:
        player.velocity = soy.atoms.Vector((-player.velocity.x, player.velocity.y, player.velocity.z  ))
        player.position = soy.atoms.Position((player.position.x + 1, player.position.y, player.position.z))
    return 0