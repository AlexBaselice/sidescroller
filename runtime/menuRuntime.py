import soy
import sys

def playerLoad(room) :

    # Player
    gold = soy.atoms.Color('#ccaaff')
    firebrick = soy.atoms.Color('firebrick')

    cubemap = soy.textures.Cubemap("checkered", [gold, firebrick], 1, 1, 1)

    room['player'] = soy.bodies.Box()
    room['player'].position = soy.atoms.Position((-60, -4, -1))
    room['player'].material = soy.materials.Textured()
    room['player'].material.colormap = cubemap
    room['player'].size = soy.atoms.Size((2, 4, 2))
    room['player'].velocity = soy.atoms.Vector((0, 0, 0))
    room['player'].angular_velocity = soy.atoms.Vector((0, 0, 0))
    return

def playerLoadEngine(room) :

    # Player Settings
    speed = 5000
    jumpvelocity = 6000
    # Forces
    Fforce = soy.atoms.Vector((0, 0, -speed))  # Forwards force
    Bforce = soy.atoms.Vector((0, 0, speed))  # Backwards force
    Rforce = soy.atoms.Vector((-speed, 0, 0))  # Right force
    Lforce = soy.atoms.Vector((speed, 0, 0))  # Left force
    Jforce = soy.atoms.Vector((0, jumpvelocity,))  # Jump Force

    # Actions
    FThrust = soy.actions.Thrust(room['player'], Fforce)
    BThrust = soy.actions.Thrust(room['player'], Bforce)
    RThrust = soy.actions.Thrust(room['player'], Rforce)
    LThrust = soy.actions.Thrust(room['player'], Lforce)
    JThrust = soy.actions.Thrust(room['player'], Jforce)

    # Player KeyPress Events
    soy.events.KeyPress.addAction("Space", JThrust)  # Jump

    return

# When the selection block gets to a corresponding play or quit y value, put a light at position of the selected block
def selectedLevel(room, menuOpen):
    # Exiting early if menu is not open
    if menuOpen == 0:
        return 0
    selectionBlockY = room['selectionBlock'].position.y
    playBlockY = room['playBlock'].position.y
    quitBlockY = room['quitBlock'].position.y

    if abs(selectionBlockY - quitBlockY) < 0.9 :
        sys.exit(0)

        #Start the game
    if abs(selectionBlockY - playBlockY) < 0.9 :
        # Set objects in non menu state - put this in its own function
        room['loadingBillboard'] = soy.bodies.Billboard(soy.atoms.Position((0, -3, 12)),
                            size=soy.atoms.Size((0, 0)), material=soy.materials.Textured())
        room['selectionBlock'].radius = 0.0
        room['playBlock'].size = soy.atoms.Size((0, 0, 0))
        room['quitBlock'].size = soy.atoms.Size((0, 0, 0))
        room['cam'].position = soy.atoms.Position((-65, -3, 18))
        if 'player' in room:
            playerLoad(room)
        else :
            playerLoad(room)
            playerLoadEngine(room)
        return 0
    else :
        return 1
    return

