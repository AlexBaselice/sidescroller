import soy
import sys

from . import endgame

#When the player presses v, switch reverse gravity, c switch to reverse gravity
def gravitySwitch(room, debug):
    # Current Y of the gravitySelectionBlock
    if (debug) :
        sys.stdout.write("gravitySelectionBlockY: " + str(gravitySelectionBlockY) + "\n")
        sys.stdout.flush()
    gravitySelectionBlockY = room['gravitySelectionBlock'].position.y
    normalGravityY = -2
    reverseGravityY = -4

    if abs(gravitySelectionBlockY - normalGravityY) < 0.95 :
        # Change Room Gravity to reverse
        if(debug) :
            sys.stdout.write("Trying to set to reverse gravity" + "\n")
            sys.stdout.flush()
        reverseGravity = soy.fields.Accelerate(soy.atoms.Vector((0, 9.8, 0)))
        room.addField('reverseGravity', reverseGravity)
        room.removeField('gravity')
        room['gravitySelectionBlock'].position = soy.atoms.Position((-4, -3, 32))
        # Change the cube color
        gold = soy.atoms.Color('#ffffff')
        firebrick = soy.atoms.Color('#008c02')
        cubemap = soy.textures.Cubemap("checkered", [gold, firebrick], 1, 1, 1)
        room['player'].material.colormap = cubemap
    if abs(gravitySelectionBlockY - reverseGravityY) < 0.95 :
        if (debug) :
            sys.stdout.write("Trying to set to normal gravity" + "\n")
            sys.stdout.flush()
        gravity = soy.fields.Accelerate(soy.atoms.Vector((0, -9.8, 0)))
        room.addField('gravity', gravity)
        room.removeField('reverseGravity')
        room['gravitySelectionBlock'].position = soy.atoms.Position((-4, -3, 32))

        reverseGravity = soy.fields.Accelerate(soy.atoms.Vector((0, -9.8, 0)))
        # Set objects in non menu state - put this in its own function
        # Change to normal color
        gold = soy.atoms.Color('#ffffff')
        firebrick = soy.atoms.Color('firebrick')
        cubemap = soy.textures.Cubemap("checkered", [gold, firebrick], 1, 1, 1)
        room['player'].material.colormap = cubemap
    return

# Functions for setting speed to zero once no keys are pressed Detecting w or s
def pressedWorS(room, previousFrameYforWorS, debug):
    # Current Y of the selectionBlockWorS
    verticalSelectionBlockY = room['selectionBlockWorS'].position.y

    playerZSpeed = 7
    # if (debug) :

    playerHasReversedDirection = 0
    if previousFrameYforWorS != -3:
        if previousFrameYforWorS > verticalSelectionBlockY and verticalSelectionBlockY > -3:
            playerHasReversedDirection =1
        if previousFrameYforWorS < verticalSelectionBlockY and verticalSelectionBlockY < -3:
            playerHasReversedDirection =1

    #adjust values to make more responsive
    if verticalSelectionBlockY > -3:
        #sys.stdout.write("Pressed W" + "\n")
        # teleport to level 2 spot
        room['player'].velocity = soy.atoms.Vector((room['player'].velocity.x, room['player'].velocity.y, -playerZSpeed))
        #sys.stdout.flush()
    if verticalSelectionBlockY < -3:
        #sys.stdout.write("Pressed S" + "\n")
        room['player'].velocity = soy.atoms.Vector((room['player'].velocity.x, room['player'].velocity.y, playerZSpeed))
        #sys.stdout.flush()
    if abs(verticalSelectionBlockY - previousFrameYforWorS) < 0.01 or playerHasReversedDirection:
        room["selectionBlockWorS"].position = soy.atoms.Position((room["selectionBlockWorS"].position.x, -3, room["selectionBlockWorS"].position.z))
        # Setting player z velocity to 0 because w or s are not pressed
        room['player'].velocity = soy.atoms.Vector((room['player'].velocity.x, room['player'].velocity.y, 0))
    return verticalSelectionBlockY

def pressedAorD(room, previousFrameYforAorD, debug):
    # Current Y of the selectionBlockWorS
    verticalSelectionBlockY = room['selectionBlockAorD'].position.y

    playerXSpeed = 5

    playerHasReversedDirection = 0
    if previousFrameYforAorD != -3:
        if previousFrameYforAorD > verticalSelectionBlockY and verticalSelectionBlockY > -3:
            playerHasReversedDirection =1
        if previousFrameYforAorD < verticalSelectionBlockY and verticalSelectionBlockY < -3:
            playerHasReversedDirection =1

    #checking if the player has just reversed direction

    #adjust values to make more responsive
    if verticalSelectionBlockY > -3 :
        room['player'].velocity = soy.atoms.Vector((-playerXSpeed, room['player'].velocity.y, room['player'].velocity.z))
    if verticalSelectionBlockY < -3 :
        room['player'].velocity = soy.atoms.Vector((playerXSpeed, room['player'].velocity.y, room['player'].velocity.z))
    if abs(verticalSelectionBlockY- previousFrameYforAorD) < 0.01 or playerHasReversedDirection:
        room['selectionBlockAorD'].position = soy.atoms.Position((-7, -3, 35))
        # Setting player x velocity to 0 because a or d are not pressed
        room['player'].velocity = soy.atoms.Vector((0, room['player'].velocity.y, room['player'].velocity.z))
    return verticalSelectionBlockY

def returnPlayerUpright(room):
    room['player'].rotation = soy.atoms.Rotation((1, 0, 0.000000, 0.000000))
    currentRotationX = room['player'].rotation.x
    currentRotationY = room['player'].rotation.y
    room['player'].angular_velocity = soy.atoms.Vector((0, 0, 0))
    return

def slowDownPlayer(room):
    drag = 0.6
    maxSpeed = 6
    if room['player'].velocity.x > maxSpeed or room['player'].velocity.y > maxSpeed or room['player'].velocity.z > maxSpeed:
        room['player'].velocity = soy.atoms.Vector((room['player'].velocity.x * drag,
                                                room['player'].velocity.y * drag,
                                                room['player'].velocity.z * drag))

    return


def checkIfPlayerCollidesWithEnemy(room, collisionRangeX, collisionRangeY, collisionRangeZ, textures, gameState, client):
    if gameState['currentLevel'] != 1:
        return
    isWithinRangeX = abs(room['player'].position.x - room['enemy'].position.x) < collisionRangeX
    isWithinRangeY = abs(room['player'].position.y - room['enemy'].position.y) < collisionRangeY
    isWithinRangeZ = abs(room['player'].position.z - room['enemy'].position.z) < collisionRangeZ
    if isWithinRangeX and isWithinRangeY and isWithinRangeZ:
        sys.stdout.write("Player has collided with enemy, quiting the game." + "\n")
        sys.stdout.flush()
        endgame.returnToMenu(room, textures, client)
        return 1
    return 0

def checkIfPlayerCollidesWithEnemy2(room, collisionRangeX, collisionRangeY, collisionRangeZ, textures, gameState, client):

    isWithinRangeX = abs(room['player'].position.x - room['enemy2'].position.x) < collisionRangeX
    isWithinRangeY = abs(room['player'].position.y - room['enemy2'].position.y) < collisionRangeY
    isWithinRangeZ = abs(room['player'].position.z - room['enemy2'].position.z) < collisionRangeZ
    if isWithinRangeX and isWithinRangeY and isWithinRangeZ:
        sys.stdout.write("Player has collided with enemy, quiting the game." + "\n")
        sys.stdout.flush()
        endgame.returnToMenu(room, textures, client)
        return 1
    return 0


def checkIfPlayerCollidesWithEnemy3(room, collisionRangeX, collisionRangeY, collisionRangeZ, textures, gameState, client):

    isWithinRangeX = abs(room['player'].position.x - room['enemy3'].position.x) < collisionRangeX
    isWithinRangeY = abs(room['player'].position.y - room['enemy3'].position.y) < collisionRangeY
    isWithinRangeZ = abs(room['player'].position.z - room['enemy3'].position.z) < collisionRangeZ
    if isWithinRangeX and isWithinRangeY and isWithinRangeZ:
        sys.stdout.write("Player has collided with enemy, quiting the game." + "\n")
        sys.stdout.flush()
        endgame.returnToMenu(room, textures, client)
        return 1
    return 0
